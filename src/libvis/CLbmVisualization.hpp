/*
 * Copyright
 * 2010 Martin Schreiber
 * 2013 Arash Bakhtiari
 * 2016 Christoph Riesinger, Ayman Saleem
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CLBMVISUALIZATION_HPP
#define CLBMVISUALIZATION_HPP

#include "../CLbmSolverCPU.hpp"
#include "../CLbmSolverGPU.cuh"

template <typename T>
class CLbmVisualization
{
protected:
    int id;
    int visualizationRate;
    Flag* flagsCPU;
    Flag* flagsGPU;
    T* densitiesCPU;
    T* densitiesGPU;
    T* velocitiesCPU;
    T* velocitiesGPU;
    CLbmSolverCPU<T>* solverCPU;
    CLbmSolverGPU<T>* solverGPU;

public:
    CLbmVisualization(int id, int visualizationRate, CLbmSolverCPU<T>* solverCPU, CLbmSolverGPU<T>* solverGPU) :
        id(id), visualizationRate(visualizationRate), solverCPU(solverCPU), solverGPU(solverGPU)
    {
        flagsCPU = new Flag[this->solverCPU->getDomain()->getNumOfCells()];
        flagsGPU = new Flag[this->solverGPU->getDomain()->getNumOfCells()];
        densitiesCPU = new T[this->solverCPU->getDomain()->getNumOfCells()];
        densitiesGPU = new T[this->solverGPU->getDomain()->getNumOfCells()];
        velocitiesCPU = new T[3 * this->solverCPU->getDomain()->getNumOfCells()];
        velocitiesGPU = new T[3 * this->solverGPU->getDomain()->getNumOfCells()];
    }

    virtual ~CLbmVisualization()
    {
        delete[] velocitiesGPU;
        delete[] velocitiesCPU;
        delete[] densitiesGPU;
        delete[] densitiesCPU;
        delete[] flagsGPU;
        delete[] flagsCPU;
    };

    virtual void render(int iteration = -1) = 0;
};

#endif
