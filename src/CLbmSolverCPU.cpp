/*
 * Copyright
 * 2010 Martin Schreiber
 * 2013 Arash Bakhtiari
 * 2016 Christoph Riesinger, Ayman Saleem
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "CLbmSolverCPU.hpp"

#include <cassert>
#include <fstream>
#include <sstream>

template <class T>
CLbmSolverCPU<T>::CLbmSolverCPU(
        int id,
        CDomain<T>& domain,
        std::vector<Flag> boundaryConditions,
        CConfiguration<T>* configuration) :
        CLbmSolver<T>(id,
                domain,
                boundaryConditions,
                configuration)
{
    if (configuration->doLogging)
    {
        std::stringstream loggingFileName;
        loggingFileName << configuration->loggingOutputDir << "/log_" << id << ".txt";
        std::ofstream loggingFile(loggingFileName.str().c_str(), std::ios::out | std::ios::app);
        if (loggingFile.is_open())
        {
            loggingFile << "----- CLbmSolverCPU<T>::CLbmSolverCPU() -----" << std::endl;
            loggingFile << "id:                                                 " << this->id << std::endl;
            loggingFile << "---------------------------------------------" << std::endl;
            loggingFile.close();
        } else {
            std::cerr << "----- CLbmSolverCPU<T>::CLbmSolverCPU() -----" << std::endl;
            std::cerr << "There is no open file to write logs." << std::endl;
            std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
            std::cerr << "---------------------------------------------" << std::endl;

            exit (EXIT_FAILURE);
        }
    }

    densityDistributions = new T[NUM_LATTICE_VECTORS * this->domain.getNumOfCellsWithHalo()];
    flags = new Flag[this->domain.getNumOfCellsWithHalo()];
    if (storeVelocities)
        velocities = new T[3 * this->domain.getNumOfCellsWithHalo()];
    if (storeDensities)
        densities = new T[this->domain.getNumOfCellsWithHalo()];

    if (configuration->doLogging) {
        std::stringstream loggingFileName;
        loggingFileName << configuration->loggingOutputDir << "/log_" << id << ".txt";
        std::ofstream loggingFile(loggingFileName.str().c_str(), std::ios::out | std::ios::app);
        if (loggingFile.is_open())
        {
            loggingFile << "boundaryConditions[0 = LEFT]:                       " << boundaryConditions[0] << std::endl;
            loggingFile << "boundaryConditions[1 = RIGHT]:                      " << boundaryConditions[1] << std::endl;
            loggingFile << "boundaryConditions[2 = BOTTOM]:                     " << boundaryConditions[2] << std::endl;
            loggingFile << "boundaryConditions[3 = TOP]:                        " << boundaryConditions[3] << std::endl;
            loggingFile << "boundaryConditions[4 = BACK]:                       " << boundaryConditions[4] << std::endl;
            loggingFile << "boundaryConditions[5 = FRONT]:                      " << boundaryConditions[5] << std::endl;
            loggingFile << "-----------------------------------" << std::endl;
            loggingFile << "size of allocated memory for density distributions: " << ((T)(NUM_LATTICE_VECTORS * this->domain.getNumOfCellsWithHalo() * sizeof(T)) / (T)(1<<20)) << " MBytes" << std::endl;
            loggingFile << "size of allocated memory for flags:                 " << ((T)(this->domain.getNumOfCellsWithHalo() * sizeof(Flag)) / (T)(1<<20)) << " MBytes" << std::endl;
            if (storeVelocities)
                loggingFile << "size of allocated memory for velocities:            " << ((T)(3 * this->domain.getNumOfCellsWithHalo() * sizeof(T)) / (T)(1<<20)) << " MBytes" << std::endl;
            if (storeDensities)
                loggingFile << "size of allocated memory for densities:             " << ((T)(this->domain.getNumOfCellsWithHalo() * sizeof(T)) / (T)(1<<20)) << " MBytes" << std::endl;
            loggingFile << "-----------------------------------" << std::endl;
            loggingFile.close();
        } else {
            std::cerr << "----- CLbmSolverCPU<T>::CLbmSolverCPU() -----" << std::endl;
            std::cerr << "There is no open file to write logs." << std::endl;
            std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
            std::cerr << "---------------------------------------------" << std::endl;

            exit (EXIT_FAILURE);
        }
    }

    initLbmCPU = new CLbmInitCPU<T>(
            domain.getSizeWithHalo(),
            boundaryConditions);
    initLbmCPU->initLbm(
            densityDistributions,
            flags,
            densities,
            velocities,
            storeDensities,
            storeVelocities);
    alphaLbmCPU = new CLbmAlphaCPU<T>(
            domain.getSizeWithHalo(),
            accelerationDimLess);
    betaLbmCPU = new CLbmBetaCPU<T>(
            domain.getSizeWithHalo(),
            accelerationDimLess);

    if (configuration->doLogging) {
        std::stringstream loggingFileName;
        loggingFileName << configuration->loggingOutputDir << "/log_" << id << ".txt";
        std::ofstream loggingFile(loggingFileName.str().c_str(), std::ios::out | std::ios::app);
        if (loggingFile.is_open())
        {
            loggingFile << "CPU domain successfully initialized." << std::endl;
            loggingFile << "---------------------------------------------" << std::endl;
            loggingFile.close();
        } else {
            std::cerr << "----- CLbmSolverCPU<T>::CLbmSolverCPU() -----" << std::endl;
            std::cerr << "There is no open file to write logs." << std::endl;
            std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
            std::cerr << "---------------------------------------------" << std::endl;

            exit (EXIT_FAILURE);
        }
    }
}

template <class T>
CLbmSolverCPU<T>::~CLbmSolverCPU()
{
    delete betaLbmCPU;
    delete alphaLbmCPU;
    delete initLbmCPU;

    if(storeDensities)
        delete[] densities;
    if(storeVelocities)
        delete[] velocities;
    delete[] flags;
    delete[] densityDistributions;
}

template <class T>
void CLbmSolverCPU<T>::getVariable(CVector<3, int>& size, CVector<3, int>& srcOrigin, CVector<3, int>& dstOrigin, CVector<3, int>& dstDim, Architecture dstArch, T* src, T* dst, cudaStream_t* stream)
{
    assert(size[0] > 0 && size[1] > 0 && size[2] > 0);
    assert(srcOrigin[0] >= 0 && srcOrigin[1] >= 0 && srcOrigin[2] >= 0);
    assert(dstOrigin[0] >= 0 && dstOrigin[1] >= 0 && dstOrigin[2] >= 0);
    assert(srcOrigin[0] + size[0] <= domain.getSizeWithHalo()[0]);
    assert(srcOrigin[1] + size[1] <= domain.getSizeWithHalo()[1]);
    assert(srcOrigin[2] + size[2] <= domain.getSizeWithHalo()[2]);
    assert(dstOrigin[0] + size[0] <= dstDim[0]);
    assert(dstOrigin[1] + size[1] <= dstDim[1]);
    assert(dstOrigin[2] + size[2] <= dstDim[2]);

    cudaMemcpy3DParms params = {0};

    // domain location and size
    params.srcPtr = make_cudaPitchedPtr(src, domain.getSizeWithHalo()[0] * sizeof(T), domain.getSizeWithHalo()[0], domain.getSizeWithHalo()[1]);
    // cuboid origin
    params.srcPos = make_cudaPos(srcOrigin[0] * (sizeof(T) / sizeof(unsigned char)), srcOrigin[1], srcOrigin[2]);
    // destination location and size
    params.dstPtr = make_cudaPitchedPtr(dst, dstDim[0] * sizeof(T), dstDim[0], dstDim[1]);
    // destination origin
    params.dstPos = make_cudaPos(dstOrigin[0] * (sizeof(T) / sizeof(unsigned char)), dstOrigin[1], dstOrigin[2]);
    // cuboid size
    params.extent = make_cudaExtent(size[0] * (sizeof(T) / sizeof(unsigned char)), size[1], size[2]);
    params.kind = (dstArch == CPU) ? cudaMemcpyHostToHost : cudaMemcpyHostToDevice;

    GPU_ERROR_CHECK(cudaMemcpy3DAsync(&params, (stream == NULL) ? 0 : *stream))
}

template <class T>
void CLbmSolverCPU<T>::getVariable(CVector<3, int>& size, CVector<3, int>& srcOrigin, CVector<3, int>& dstOrigin, CVector<3, int>& dstDim, Architecture dstArch, Flag* src, Flag* dst, cudaStream_t* stream)
{
    assert(size[0] > 0 && size[1] > 0 && size[2] > 0);
    assert(srcOrigin[0] >= 0 && srcOrigin[1] >= 0 && srcOrigin[2] >= 0);
    assert(dstOrigin[0] >= 0 && dstOrigin[1] >= 0 && dstOrigin[2] >= 0);
    assert(srcOrigin[0] + size[0] <= domain.getSizeWithHalo()[0]);
    assert(srcOrigin[1] + size[1] <= domain.getSizeWithHalo()[1]);
    assert(srcOrigin[2] + size[2] <= domain.getSizeWithHalo()[2]);
    assert(dstOrigin[0] + size[0] <= dstDim[0]);
    assert(dstOrigin[1] + size[1] <= dstDim[1]);
    assert(dstOrigin[2] + size[2] <= dstDim[2]);

    cudaMemcpy3DParms params = {0};

    // domain location and size
    params.srcPtr = make_cudaPitchedPtr(src, domain.getSizeWithHalo()[0] * sizeof(Flag), domain.getSizeWithHalo()[0], domain.getSizeWithHalo()[1]);
    // cuboid origin
    params.srcPos = make_cudaPos(srcOrigin[0] * (sizeof(Flag) / sizeof(unsigned char)), srcOrigin[1], srcOrigin[2]);
    // destination location and size
    params.dstPtr = make_cudaPitchedPtr(dst, dstDim[0] * sizeof(Flag), dstDim[0], dstDim[1]);
    // destination origin
    params.dstPos = make_cudaPos(dstOrigin[0] * (sizeof(Flag) / sizeof(unsigned char)), dstOrigin[1], dstOrigin[2]);
    // cuboid size
    params.extent = make_cudaExtent(size[0] * (sizeof(Flag) / sizeof(unsigned char)), size[1], size[2]);
    params.kind = (dstArch == CPU) ? cudaMemcpyHostToHost : cudaMemcpyHostToDevice;

    GPU_ERROR_CHECK(cudaMemcpy3DAsync(&params, (stream == NULL) ? 0 : *stream))
}

template <class T>
void CLbmSolverCPU<T>::setVariable(CVector<3, int>& size, CVector<3, int>& srcOrigin, CVector<3, int>& srcDim, Architecture srcArch, CVector<3, int>& dstOrigin, T* src, T* dst, cudaStream_t* stream)
{
    assert(size[0] > 0 && size[1] > 0 && size[2] > 0);
    assert(srcOrigin[0] >= 0 && srcOrigin[1] >= 0 && srcOrigin[2] >= 0);
    assert(dstOrigin[0] >= 0 && dstOrigin[1] >= 0 && dstOrigin[2] >= 0);
    assert(srcOrigin[0] + size[0] <= srcDim[0]);
    assert(srcOrigin[1] + size[1] <= srcDim[1]);
    assert(srcOrigin[2] + size[2] <= srcDim[2]);
    assert(dstOrigin[0] + size[0] <= domain.getSizeWithHalo()[0]);
    assert(dstOrigin[1] + size[1] <= domain.getSizeWithHalo()[1]);
    assert(dstOrigin[2] + size[2] <= domain.getSizeWithHalo()[2]);

    cudaMemcpy3DParms params = {0};

    // source location and size
    params.srcPtr = make_cudaPitchedPtr(src, srcDim[0] * sizeof(T), srcDim[0], srcDim[1]);
    // source origin
    params.srcPos = make_cudaPos(srcOrigin[0] * (sizeof(T) / sizeof(unsigned char)), srcOrigin[1], srcOrigin[2]);
    // domain location and size
    params.dstPtr = make_cudaPitchedPtr(dst, domain.getSizeWithHalo()[0] * sizeof(T), domain.getSizeWithHalo()[0], domain.getSizeWithHalo()[1]);
    // cuboid origin
    params.dstPos = make_cudaPos(dstOrigin[0] * (sizeof(T) / sizeof(unsigned char)), dstOrigin[1], dstOrigin[2]);
    // cuboid size
    params.extent = make_cudaExtent(size[0] * (sizeof(T) / sizeof(unsigned char)), size[1], size[2]);
    params.kind = (srcArch == CPU) ? cudaMemcpyHostToHost : cudaMemcpyDeviceToHost;

    GPU_ERROR_CHECK(cudaMemcpy3DAsync(&params, (stream == NULL) ? 0 : *stream))
}

template <class T>
void CLbmSolverCPU<T>::setVariable(CVector<3, int>& size, CVector<3, int>& srcOrigin, CVector<3, int>& srcDim, Architecture srcArch, CVector<3, int>& dstOrigin, Flag* src, Flag* dst, cudaStream_t* stream)
{
    assert(size[0] > 0 && size[1] > 0 && size[2] > 0);
    assert(srcOrigin[0] >= 0 && srcOrigin[1] >= 0 && srcOrigin[2] >= 0);
    assert(dstOrigin[0] >= 0 && dstOrigin[1] >= 0 && dstOrigin[2] >= 0);
    assert(srcOrigin[0] + size[0] <= srcDim[0]);
    assert(srcOrigin[1] + size[1] <= srcDim[1]);
    assert(srcOrigin[2] + size[2] <= srcDim[2]);
    assert(dstOrigin[0] + size[0] <= domain.getSizeWithHalo()[0]);
    assert(dstOrigin[1] + size[1] <= domain.getSizeWithHalo()[1]);
    assert(dstOrigin[2] + size[2] <= domain.getSizeWithHalo()[2]);

    cudaMemcpy3DParms params = {0};

    // source location and size
    params.srcPtr = make_cudaPitchedPtr(src, srcDim[0] * sizeof(Flag), srcDim[0], srcDim[1]);
    // source origin
    params.srcPos = make_cudaPos(srcOrigin[0] * (sizeof(Flag) / sizeof(unsigned char)), srcOrigin[1], srcOrigin[2]);
    // domain location and size
    params.dstPtr = make_cudaPitchedPtr(dst, domain.getSizeWithHalo()[0] * sizeof(Flag), domain.getSizeWithHalo()[0], domain.getSizeWithHalo()[1]);
    // cuboid origin
    params.dstPos = make_cudaPos(dstOrigin[0] * (sizeof(Flag) / sizeof(unsigned char)), dstOrigin[1], dstOrigin[2]);
    // cuboid size
    params.extent = make_cudaExtent(size[0] * (sizeof(Flag) / sizeof(unsigned char)), size[1], size[2]);
    params.kind = (srcArch == CPU) ? cudaMemcpyHostToHost : cudaMemcpyDeviceToHost;

    GPU_ERROR_CHECK(cudaMemcpy3DAsync(&params, (stream == NULL) ? 0 : *stream))
}

template <class T>
void CLbmSolverCPU<T>::simulationStepAlpha()
{
    if (configuration->doLogging)
    {
        std::stringstream loggingFileName;
        loggingFileName << configuration->loggingOutputDir << "/log_" << id << ".txt";
        std::ofstream loggingFile(loggingFileName.str().c_str(), std::ios::out | std::ios::app);
        if (loggingFile.is_open())
        {
            loggingFile << "----- CLbmSolverCPU<T>::simulationStepAlpha() -----" << std::endl;
            loggingFile << "id:                " << id << std::endl;
            loggingFile << "---------------------------------------------------" << std::endl;
            loggingFile.close();
        } else {
            std::cerr << "----- CLbmSolverCPU<T>::simulationStepAlpha() -----" << std::endl;
            std::cerr << "There is no open file to write logs." << std::endl;
            std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
            std::cerr << "---------------------------------------------------" << std::endl;

            exit (EXIT_FAILURE);
        }
    }

    alphaLbmCPU->alphaKernelCPU(
            densityDistributions,
            flags,
            densities,
            velocities,
            tauInv,
            velocityDimLess[0],
            CVector<3, int>(0, 0, 0),
            domain.getSizeWithHalo(),
            configuration->elementsPerBlock[0],
            storeDensities,
            storeVelocities);

    if (configuration->doLogging)
    {
        std::stringstream loggingFileName;
        loggingFileName << configuration->loggingOutputDir << "/log_" << id << ".txt";
        std::ofstream loggingFile(loggingFileName.str().c_str(), std::ios::out | std::ios::app);
        if (loggingFile.is_open())
        {
            loggingFile << "Alpha kernel was successfully executed on the whole CPU subdomain." << std::endl;
            loggingFile << "---------------------------------------------------" << std::endl;
            loggingFile.close();
        } else {
            std::cerr << "----- CLbmSolverGCU<T>::simulationStepAlpha() -----" << std::endl;
            std::cerr << "There is no open file to write logs." << std::endl;
            std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
            std::cerr << "---------------------------------------------------" << std::endl;

            exit (EXIT_FAILURE);
        }
    }
}

template <class T>
void CLbmSolverCPU<T>::simulationStepAlpha(CVector<3, int> origin, CVector<3, int> size)
{
    assert(origin[0] >= 0 && origin[1] >= 0 && origin[2] >= 0);
    assert(size[0] > 0 && size[1] > 0 && size[2] > 0);
    assert(origin[0] + size[0] <= domain.getSizeWithHalo()[0]);
    assert(origin[1] + size[1] <= domain.getSizeWithHalo()[1]);
    assert(origin[2] + size[2] <= domain.getSizeWithHalo()[2]);

    if (configuration->doLogging)
    {
        std::stringstream loggingFileName;
        loggingFileName << configuration->loggingOutputDir << "/log_" << id << ".txt";
        std::ofstream loggingFile(loggingFileName.str().c_str(), std::ios::out | std::ios::app);
        if (loggingFile.is_open())
        {
            loggingFile << "----- CLbmSolverCPU<T>::simulationStepAlpha() -----" << std::endl;
            loggingFile << "id:                " << id << std::endl;
            loggingFile << "---------------------------------------------------" << std::endl;
            loggingFile.close();
        } else {
            std::cerr << "----- CLbmSolverCPU<T>::simulationStepAlpha() -----" << std::endl;
            std::cerr << "There is no open file to write logs." << std::endl;
            std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
            std::cerr << "---------------------------------------------------" << std::endl;

            exit (EXIT_FAILURE);
        }
    }

    alphaLbmCPU->alphaKernelCPU(
            densityDistributions,
            flags,
            densities,
            velocities,
            tauInv,
            velocityDimLess[0],
            origin,
            size,
            configuration->elementsPerBlock[0],
            storeDensities,
            storeVelocities);

    if (configuration->doLogging)
    {
        std::stringstream loggingFileName;
        loggingFileName << configuration->loggingOutputDir << "/log_" << id << ".txt";
        std::ofstream loggingFile(loggingFileName.str().c_str(), std::ios::out | std::ios::app);
        if (loggingFile.is_open())
        {
            loggingFile << "Alpha kernel was successfully executed on the following CPU subdomain:" << std::endl;
            loggingFile << "origin:            " << origin << std::endl;
            loggingFile << "size:              " << size << std::endl;
            loggingFile << "---------------------------------------------------" << std::endl;
            loggingFile.close();
        } else {
            std::cerr << "----- CLbmSolverCPU<T>::simulationStepAlpha() -----" << std::endl;
            std::cerr << "There is no open file to write logs." << std::endl;
            std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
            std::cerr << "---------------------------------------------------" << std::endl;

            exit (EXIT_FAILURE);
        }
    }
}

template <class T>
void CLbmSolverCPU<T>::simulationStepBeta()
{
    if (configuration->doLogging)
    {
        std::stringstream loggingFileName;
        loggingFileName << configuration->loggingOutputDir << "/log_" << id << ".txt";
        std::ofstream loggingFile(loggingFileName.str().c_str(), std::ios::out | std::ios::app);
        if (loggingFile.is_open())
        {
            loggingFile << "----- CLbmSolverCPU<T>::simulationStepBeta() -----" << std::endl;
            loggingFile << "id:                 " << id << std::endl;
            loggingFile << "--------------------------------------------------" << std::endl;
            loggingFile.close();
        } else {
            std::cerr << "----- CLbmSolverCPU<T>::simulationStepBeta() -----" << std::endl;
            std::cerr << "There is no open file to write logs." << std::endl;
            std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
            std::cerr << "--------------------------------------------------" << std::endl;

            exit (EXIT_FAILURE);
        }
    }

    betaLbmCPU->betaKernelCPU(
            densityDistributions,
            flags,
            densities,
            velocities,
            tauInv,
            velocityDimLess[0],
            CVector<3, int>(0, 0, 0),
            domain.getSizeWithHalo(),
            configuration->elementsPerBlock[1],
            storeDensities,
            storeVelocities);

    if (configuration->doLogging)
    {
        std::stringstream loggingFileName;
        loggingFileName << configuration->loggingOutputDir << "/log_" << id << ".txt";
        std::ofstream loggingFile(loggingFileName.str().c_str(), std::ios::out | std::ios::app);
        if (loggingFile.is_open())
        {
            loggingFile << "Beta kernel was successfully executed on the whole CPU subdomain." << std::endl;
            loggingFile << "--------------------------------------------------" << std::endl;
            loggingFile.close();
        } else {
            std::cerr << "----- CLbmSolverCPU<T>::simulationStepBeta() -----" << std::endl;
            std::cerr << "There is no open file to write logs." << std::endl;
            std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
            std::cerr << "--------------------------------------------------" << std::endl;

            exit (EXIT_FAILURE);
        }
    }
}

template <class T>
void CLbmSolverCPU<T>::simulationStepBeta(CVector<3, int> origin, CVector<3, int> size)
{
    assert(origin[0] >= 0 && origin[1] >= 0 && origin[2] >= 0);
    assert(size[0] > 0 && size[1] > 0 && size[2] > 0);
    assert(origin[0] + size[0] <= domain.getSizeWithHalo()[0]);
    assert(origin[1] + size[1] <= domain.getSizeWithHalo()[1]);
    assert(origin[2] + size[2] <= domain.getSizeWithHalo()[2]);

    if (configuration->doLogging)
    {
        std::stringstream loggingFileName;
        loggingFileName << configuration->loggingOutputDir << "/log_" << id << ".txt";
        std::ofstream loggingFile(loggingFileName.str().c_str(), std::ios::out | std::ios::app);
        if (loggingFile.is_open())
        {
            loggingFile << "----- CLbmSolverCPU<T>::simulationStepBeta() -----" << std::endl;
            loggingFile << "id:                 " << id << std::endl;
            loggingFile << "--------------------------------------------------" << std::endl;
            loggingFile.close();
        } else {
            std::cerr << "----- CLbmSolverCPU<T>::simulationStepBeta() -----" << std::endl;
            std::cerr << "There is no open file to write logs." << std::endl;
            std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
            std::cerr << "--------------------------------------------------" << std::endl;

            exit (EXIT_FAILURE);
        }
    }

    betaLbmCPU->betaKernelCPU(
            densityDistributions,
            flags,
            densities,
            velocities,
            tauInv,
            velocityDimLess[0],
            origin,
            size,
            configuration->elementsPerBlock[1],
            storeDensities,
            storeVelocities);

    if (configuration->doLogging)
    {
        std::stringstream loggingFileName;
        loggingFileName << configuration->loggingOutputDir << "/log_" << id << ".txt";
        std::ofstream loggingFile(loggingFileName.str().c_str(), std::ios::out | std::ios::app);
        if (loggingFile.is_open())
        {
            loggingFile << "Beta kernel was successfully executed on the following CPU subdomain." << std::endl;
            loggingFile << "origin:             " << origin << std::endl;
            loggingFile << "size:               " << size << std::endl;
            loggingFile << "--------------------------------------------------" << std::endl;
            loggingFile.close();
        } else {
            std::cerr << "----- CLbmSolverCPU<T>::simulationStepBeta() -----" << std::endl;
            std::cerr << "There is no open file to write logs." << std::endl;
            std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
            std::cerr << "--------------------------------------------------" << std::endl;

            exit (EXIT_FAILURE);
        }
    }
}

template <class T>
void CLbmSolverCPU<T>::getDensityDistributions(CVector<3, int>& origin, CVector<3, int>& size, Architecture dstArch, T* dst, cudaStream_t* stream)
{
    if (configuration->doLogging)
    {
        std::stringstream loggingFileName;
        loggingFileName << configuration->loggingOutputDir << "/log_" << id << ".txt";
        std::ofstream loggingFile(loggingFileName.str().c_str(), std::ios::out | std::ios::app);
        if (loggingFile.is_open())
        {
            loggingFile << "----- CLbmSolverCPU<T>::getDensityDistributions() -----" << std::endl;
            loggingFile << "id:                " << id << std::endl;
            loggingFile << "-------------------------------------------------------" << std::endl;
            loggingFile << "domain origin:     " << domain.getOrigin() << std::endl;
            loggingFile << "domain size:       " << domain.getSize() << std::endl;
            loggingFile << "-------------------------------------------------------" << std::endl;
            loggingFile << "cuboid origin:     " << origin << std::endl;
            loggingFile << "cuboid size:       " << size << std::endl;
            loggingFile << "-------------------------------------------------------" << std::endl;
            loggingFile.close();
        } else {
            std::cerr << "----- CLbmSolverCPU<T>::getDensityDistributions() -----" << std::endl;
            std::cerr << "There is no open file to write logs." << std::endl;
            std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
            std::cerr << "-------------------------------------------------------" << std::endl;

            exit (EXIT_FAILURE);
        }
    }

    CVector<3, int> dstOrigin(0);

    for(int latticeVector = 0; latticeVector < NUM_LATTICE_VECTORS; latticeVector++)
    {
        getVariable(size, origin, dstOrigin, size, dstArch, &densityDistributions[latticeVector * domain.getNumOfCellsWithHalo()], &dst[latticeVector * size.elements()], stream);
    }

    if (configuration->doLogging)
    {
        std::stringstream loggingFileName;
        loggingFileName << configuration->loggingOutputDir << "/log_" << id << ".txt";
        std::ofstream loggingFile(loggingFileName.str().c_str(), std::ios::out | std::ios::app);
        if (loggingFile.is_open())
        {
            loggingFile << "A copy operation from host to " << ((dstArch == CPU) ? "host" : "device") << " was performed." << std::endl;
            loggingFile << "-------------------------------------------------------" << std::endl;
            loggingFile.close();
        } else {
            std::cerr << "----- CLbmSolverCPU<T>::getDensityDistributions() -----" << std::endl;
            std::cerr << "There is no open file to write logs." << std::endl;
            std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
            std::cerr << "-------------------------------------------------------" << std::endl;

            exit (EXIT_FAILURE);
        }
    }
}

template <class T>
void CLbmSolverCPU<T>::getDensityDistributions(Architecture dstArch, T* dst, cudaStream_t* stream)
{
    CVector<3, int> origin(1);
    CVector<3, int> size(domain.getSize());

    getDensityDistributions(origin, size, dstArch, dst, stream);
}

template <class T>
void CLbmSolverCPU<T>::setDensityDistributions(CVector<3, int>& size, CVector<3, int>& srcOrigin, CVector<3, int>& srcDim, Architecture srcArch, CVector<3, int>& dstOrigin, Direction direction, T* src, cudaStream_t* stream)
{
    CVector<3, int> norm(0);

    switch(direction)
    {
    case LEFT:
        norm[0] = 1;
        break;
    case RIGHT:
        norm[0] = -1;
        break;
    case BOTTOM:
        norm[1] = 1;
        break;
    case TOP:
        norm[1] = -1;
        break;
    case BACK:
        norm[2] = 1;
        break;
    case FRONT:
        norm[2] = -1;
        break;
    }

    if (configuration->doLogging)
    {
        std::stringstream loggingFileName;
        loggingFileName << configuration->loggingOutputDir << "/log_" << id << ".txt";
        std::ofstream loggingFile(loggingFileName.str().c_str(), std::ios::out | std::ios::app);
        if (loggingFile.is_open())
        {
            loggingFile << "----- CLbmSolverCPU<T>::setDensityDistributions() -----" << std::endl;
            loggingFile << "id:                 " << id << std::endl;
            loggingFile << "-------------------------------------------------------" << std::endl;
            loggingFile << "domain origin:      " << domain.getOrigin() << std::endl;
            loggingFile << "domain size:        " << domain.getSize() << std::endl;
            loggingFile << "-------------------------------------------------------" << std::endl;
            loggingFile << "source origin:      " << srcOrigin << std::endl;
            loggingFile << "source dim:         " << srcDim << std::endl;
            loggingFile << "destination origin: " << dstOrigin << std::endl;
            loggingFile << "cuboid size:        " << size << std::endl;
            loggingFile << "direction:          " << norm << std::endl;
            loggingFile << "-------------------------------------------------------" << std::endl;
            loggingFile.close();
        } else {
            std::cerr << "----- CLbmSolverCPU<T>::setDensityDistributions() -----" << std::endl;
            std::cerr << "There is no open file to write logs." << std::endl;
            std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
            std::cerr << "-------------------------------------------------------" << std::endl;

            exit (EXIT_FAILURE);
        }
    }

    for (int latticeVector = 0; latticeVector < NUM_LATTICE_VECTORS; latticeVector++)
    {
        if(norm.dotProd(lbm_units[latticeVector]) > 0)
            setVariable(size, srcOrigin, srcDim, srcArch, dstOrigin, &src[latticeVector * srcDim.elements()], &densityDistributions[latticeVector * domain.getNumOfCellsWithHalo()], stream);
    }

    if (configuration->doLogging)
    {
        std::stringstream loggingFileName;
        loggingFileName << configuration->loggingOutputDir << "/log_" << id << ".txt";
        std::ofstream loggingFile(loggingFileName.str().c_str(), std::ios::out | std::ios::app);
        if (loggingFile.is_open())
        {
            loggingFile << "A copy operation from " << ((srcArch == CPU) ? "host" : "device") << " to host for lattice vectors in direction " << direction << " was performed." << std::endl;
            loggingFile << "-------------------------------------------------------" << std::endl;
            loggingFile.close();
        } else {
            std::cerr << "----- CLbmSolverCPU<T>::setDensityDistributions() -----" << std::endl;
            std::cerr << "There is no open file to write logs." << std::endl;
            std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
            std::cerr << "-------------------------------------------------------" << std::endl;

            exit (EXIT_FAILURE);
        }
    }
}

template <class T>
void CLbmSolverCPU<T>::setDensityDistributions(CVector<3, int>& size, CVector<3, int>& srcOrigin, CVector<3, int>& srcDim, Architecture srcArch, CVector<3, int>& dstOrigin, T* src, cudaStream_t* stream)
{
    if (configuration->doLogging)
    {
        std::stringstream loggingFileName;
        loggingFileName << configuration->loggingOutputDir << "/log_" << id << ".txt";
        std::ofstream loggingFile(loggingFileName.str().c_str(), std::ios::out | std::ios::app);
        if (loggingFile.is_open())
        {
            loggingFile << "----- CLbmSolverCPU<T>::setDensityDistributions() -----" << std::endl;
            loggingFile << "id:                 " << id << std::endl;
            loggingFile << "-------------------------------------------------------" << std::endl;
            loggingFile << "domain origin:      " << domain.getOrigin() << std::endl;
            loggingFile << "domain size:        " << domain.getSize() << std::endl;
            loggingFile << "-------------------------------------------------------" << std::endl;
            loggingFile << "source origin:      " << srcOrigin << std::endl;
            loggingFile << "source dim:         " << srcDim << std::endl;
            loggingFile << "destination origin: " << dstOrigin << std::endl;
            loggingFile << "cuboid size:        " << size << std::endl;
            loggingFile << "-------------------------------------------------------" << std::endl;
            loggingFile.close();
        } else {
            std::cerr << "----- CLbmSolverCPU<T>::setDensityDistributions() -----" << std::endl;
            std::cerr << "There is no open file to write logs." << std::endl;
            std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
            std::cerr << "-------------------------------------------------------" << std::endl;

            exit (EXIT_FAILURE);
        }
    }

    for (int latticeVector = 0; latticeVector < NUM_LATTICE_VECTORS; latticeVector++)
        setVariable(size, srcOrigin, srcDim, srcArch, dstOrigin, &src[latticeVector * srcDim.elements()], &densityDistributions[latticeVector * domain.getNumOfCellsWithHalo()], stream);

    if (configuration->doLogging)
    {
        std::stringstream loggingFileName;
        loggingFileName << configuration->loggingOutputDir << "/log_" << id << ".txt";
        std::ofstream loggingFile(loggingFileName.str().c_str(), std::ios::out | std::ios::app);
        if (loggingFile.is_open())
        {
            loggingFile << "A copy operation from " << ((srcArch == CPU) ? "host" : "device") << " to host was performed." << std::endl;
            loggingFile << "-------------------------------------------------------" << std::endl;
            loggingFile.close();
        } else {
            std::cerr << "----- CLbmSolverCPU<T>::setDensityDistributions() -----" << std::endl;
            std::cerr << "There is no open file to write logs." << std::endl;
            std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
            std::cerr << "-------------------------------------------------------" << std::endl;

            exit (EXIT_FAILURE);
        }
    }
}

template <class T>
void CLbmSolverCPU<T>::setDensityDistributions(CVector<3, int>& origin, CVector<3, int>& size, Architecture srcArch, Direction direction, T* src, cudaStream_t* stream)
{
    CVector<3, int> srcOrigin(0);

    setDensityDistributions(size, srcOrigin, size, srcArch, origin, direction, src, stream);
}

template <class T>
void CLbmSolverCPU<T>::setDensityDistributions(CVector<3, int>& origin, CVector<3, int>& size, Architecture srcArch, T* src, cudaStream_t* stream)
{
    if (configuration->doLogging)
    {
        std::stringstream loggingFileName;
        loggingFileName << configuration->loggingOutputDir << "/log_" << id << ".txt";
        std::ofstream loggingFile(loggingFileName.str().c_str(), std::ios::out | std::ios::app);
        if (loggingFile.is_open())
        {
            loggingFile << "----- CLbmSolverCPU<T>::setDensityDistributions() -----" << std::endl;
            loggingFile << "id:                " << id << std::endl;
            loggingFile << "-------------------------------------------------------" << std::endl;
            loggingFile << "domain origin:     " << domain.getOrigin() << std::endl;
            loggingFile << "domain size:       " << domain.getSize() << std::endl;
            loggingFile << "-------------------------------------------------------" << std::endl;
            loggingFile << "cuboid origin:     " << origin << std::endl;
            loggingFile << "cuboid size:       " << size << std::endl;
            loggingFile << "-------------------------------------------------------" << std::endl;
            loggingFile.close();
        } else {
            std::cerr << "----- CLbmSolverCPU<T>::setDensityDistributions() -----" << std::endl;
            std::cerr << "There is no open file to write logs." << std::endl;
            std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
            std::cerr << "-------------------------------------------------------" << std::endl;

            exit (EXIT_FAILURE);
        }
    }

    CVector<3, int> srcOrigin(0);

    for (int latticeVector = 0; latticeVector < NUM_LATTICE_VECTORS; latticeVector++)
        setVariable(size, srcOrigin, size, srcArch, origin, &src[latticeVector * size.elements()], &densityDistributions[latticeVector * domain.getNumOfCellsWithHalo()], stream);

    if (configuration->doLogging)
    {
        std::stringstream loggingFileName;
        loggingFileName << configuration->loggingOutputDir << "/log_" << id << ".txt";
        std::ofstream loggingFile(loggingFileName.str().c_str(), std::ios::out | std::ios::app);
        if (loggingFile.is_open())
        {
            loggingFile << "A copy operation from " << ((srcArch == CPU) ? "host" : "device") << " to host was performed." << std::endl;
            loggingFile << "-------------------------------------------------------" << std::endl;
            loggingFile.close();
        } else {
            std::cerr << "----- CLbmSolverCPU<T>::setDensityDistributions() -----" << std::endl;
            std::cerr << "There is no open file to write logs." << std::endl;
            std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
            std::cerr << "-------------------------------------------------------" << std::endl;

            exit (EXIT_FAILURE);
        }
    }
}

template <class T>
void CLbmSolverCPU<T>::setDensityDistributions(Architecture srcArch, T* src, cudaStream_t* stream)
{
    CVector<3, int> origin(1);
    CVector<3, int> size(domain.getSize());

    setDensityDistributions(origin, size, srcArch, src, stream);
}

template <class T>
void CLbmSolverCPU<T>::getFlags(CVector<3, int>& origin, CVector<3, int>& size, Architecture dstArch, Flag* dst)
{
    if (configuration->doLogging)
    {
        std::stringstream loggingFileName;
        loggingFileName << configuration->loggingOutputDir << "/log_" << id << ".txt";
        std::ofstream loggingFile(loggingFileName.str().c_str(), std::ios::out | std::ios::app);
        if (loggingFile.is_open())
        {
            loggingFile << "----- CLbmSolverCPU<T>::getFlags() -----" << std::endl;
            loggingFile << "id:                " << id << std::endl;
            loggingFile << "----------------------------------------" << std::endl;
            loggingFile << "domain origin:     " << domain.getOrigin() << std::endl;
            loggingFile << "domain size:       " << domain.getSize() << std::endl;
            loggingFile << "----------------------------------------" << std::endl;
            loggingFile << "cuboid origin:     " << origin << std::endl;
            loggingFile << "cuboid size:       " << size << std::endl;
            loggingFile << "----------------------------------------" << std::endl;
            loggingFile.close();
        } else {
            std::cerr << "----- CLbmSolverCPU<T>::getFlags() -----" << std::endl;
            std::cerr << "There is no open file to write logs." << std::endl;
            std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
            std::cerr << "----------------------------------------" << std::endl;

            exit (EXIT_FAILURE);
        }
    }

    CVector<3, int> dstOrigin(0);

    getVariable(size, origin, dstOrigin, size, dstArch, flags, dst, NULL);

    if (configuration->doLogging)
    {
        std::stringstream loggingFileName;
        loggingFileName << configuration->loggingOutputDir << "/log_" << id << ".txt";
        std::ofstream loggingFile(loggingFileName.str().c_str(), std::ios::out | std::ios::app);
        if (loggingFile.is_open())
        {
            loggingFile << "A copy operation from host to " << ((dstArch == CPU) ? "host" : "device") << " was performed." << std::endl;
            loggingFile << "----------------------------------------" << std::endl;
            loggingFile.close();
        } else {
            std::cerr << "----- CLbmSolverCPU<T>::getFlags() -----" << std::endl;
            std::cerr << "There is no open file to write logs." << std::endl;
            std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
            std::cerr << "----------------------------------------" << std::endl;

            exit (EXIT_FAILURE);
        }
    }
}

template <class T>
void CLbmSolverCPU<T>::getFlags(Architecture dstArch, Flag* dst)
{
    CVector<3, int> origin(1);
    CVector<3, int> size(domain.getSize());

    getFlags(origin, size, dstArch, dst);
}

template <class T>
void CLbmSolverCPU<T>::setFlags(CVector<3, int>& origin, CVector<3, int>& size, Architecture srcArch, Flag* src)
{
    if (configuration->doLogging)
    {
        std::stringstream loggingFileName;
        loggingFileName << configuration->loggingOutputDir << "/log_" << id << ".txt";
        std::ofstream loggingFile(loggingFileName.str().c_str(), std::ios::out | std::ios::app);
        if (loggingFile.is_open())
        {
            loggingFile << "----- CLbmSolverCPU<T>::setFlags() -----" << std::endl;
            loggingFile << "id:                " << id << std::endl;
            loggingFile << "----------------------------------------" << std::endl;
            loggingFile << "domain origin:     " << domain.getOrigin() << std::endl;
            loggingFile << "domain size:       " << domain.getSize() << std::endl;
            loggingFile << "----------------------------------------" << std::endl;
            loggingFile << "cuboid origin:     " << origin << std::endl;
            loggingFile << "cuboid size:       " << size << std::endl;
            loggingFile << "----------------------------------------" << std::endl;
            loggingFile.close();
        } else {
            std::cerr << "----- CLbmSolverCPU<T>::setFlags() -----" << std::endl;
            std::cerr << "There is no open file to write logs." << std::endl;
            std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
            std::cerr << "----------------------------------------" << std::endl;

            exit (EXIT_FAILURE);
        }
    }

    CVector<3, int> srcOrigin(0);

    setVariable(size, srcOrigin, size, srcArch, origin, src, flags, NULL);

    if (configuration->doLogging)
    {
        std::stringstream loggingFileName;
        loggingFileName << configuration->loggingOutputDir << "/log_" << id << ".txt";
        std::ofstream loggingFile(loggingFileName.str().c_str(), std::ios::out | std::ios::app);
        if (loggingFile.is_open())
        {
            loggingFile << "A copy operation from " << ((srcArch == CPU) ? "host" : "device") << " to host was performed." << std::endl;
            loggingFile << "----------------------------------------" << std::endl;
            loggingFile.close();
        } else {
            std::cerr << "----- CLbmSolverCPU<T>::setFlags() -----" << std::endl;
            std::cerr << "There is no open file to write logs." << std::endl;
            std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
            std::cerr << "----------------------------------------" << std::endl;

            exit (EXIT_FAILURE);
        }
    }
}

template <class T>
void CLbmSolverCPU<T>::setFlags(Architecture srcArch, Flag* src)
{
    CVector<3, int> origin(1);
    CVector<3, int> size(domain.getSize());

    setFlags(origin, size, srcArch, src);
}

template <class T>
void CLbmSolverCPU<T>::getVelocities(CVector<3, int>& origin, CVector<3, int>& size, Architecture dstArch, T* dst)
{
    if (configuration->doLogging)
    {
        std::stringstream loggingFileName;
        loggingFileName << configuration->loggingOutputDir << "/log_" << id << ".txt";
        std::ofstream loggingFile(loggingFileName.str().c_str(), std::ios::out | std::ios::app);
        if (loggingFile.is_open())
        {
            loggingFile << "----- CLbmSolverCPU<T>::getVelocities() -----" << std::endl;
            loggingFile << "id:                " << id << std::endl;
            loggingFile << "---------------------------------------------" << std::endl;
            loggingFile << "domain origin:     " << domain.getOrigin() << std::endl;
            loggingFile << "domain size:       " << domain.getSize() << std::endl;
            loggingFile << "---------------------------------------------" << std::endl;
            loggingFile << "cuboid origin:     " << origin << std::endl;
            loggingFile << "cuboid size:       " << size << std::endl;
            loggingFile << "---------------------------------------------" << std::endl;
            loggingFile.close();
        } else {
            std::cerr << "----- CLbmSolverCPU<T>::getVelocities() -----" << std::endl;
            std::cerr << "There is no open file to write logs." << std::endl;
            std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
            std::cerr << "---------------------------------------------" << std::endl;

            exit (EXIT_FAILURE);
        }
    }

    CVector<3, int> dstOrigin(0);

    for(int i = 0; i < 3; i++)
    {
        getVariable(size, origin, dstOrigin, size, dstArch, &velocities[i * domain.getNumOfCellsWithHalo()], &dst[i * size.elements()], NULL);
    }

    if (configuration->doLogging)
    {
        std::stringstream loggingFileName;
        loggingFileName << configuration->loggingOutputDir << "/log_" << id << ".txt";
        std::ofstream loggingFile(loggingFileName.str().c_str(), std::ios::out | std::ios::app);
        if (loggingFile.is_open())
        {
            loggingFile << "A copy operation from host to " << ((dstArch == CPU) ? "host" : "device") << " was performed." << std::endl;
            loggingFile << "---------------------------------------------" << std::endl;
            loggingFile.close();
        } else {
            std::cerr << "----- CLbmSolverCPU<T>::getVelocities() -----" << std::endl;
            std::cerr << "There is no open file to write logs." << std::endl;
            std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
            std::cerr << "---------------------------------------------" << std::endl;

            exit (EXIT_FAILURE);
        }
    }
}

template <class T>
void CLbmSolverCPU<T>::getVelocities(Architecture dstArch, T* dst)
{
    CVector<3, int> origin(1);
    CVector<3, int> size(domain.getSize());

    getVelocities(origin, size, dstArch, dst);
}

template <class T>
void CLbmSolverCPU<T>::setVelocities(CVector<3, int>& origin, CVector<3, int>& size, Architecture srcArch, T* src)
{
    if (configuration->doLogging)
    {
        std::stringstream loggingFileName;
        loggingFileName << configuration->loggingOutputDir << "/log_" << id << ".txt";
        std::ofstream loggingFile(loggingFileName.str().c_str(), std::ios::out | std::ios::app);
        if (loggingFile.is_open())
        {
            loggingFile << "----- CLbmSolverCPU<T>::setVelocities() -----" << std::endl;
            loggingFile << "id:                " << id << std::endl;
            loggingFile << "---------------------------------------------" << std::endl;
            loggingFile << "domain origin:     " << domain.getOrigin() << std::endl;
            loggingFile << "domain size:       " << domain.getSize() << std::endl;
            loggingFile << "---------------------------------------------" << std::endl;
            loggingFile << "cuboid origin:     " << origin << std::endl;
            loggingFile << "cuboid size:       " << size << std::endl;
            loggingFile << "---------------------------------------------" << std::endl;
            loggingFile.close();
        } else {
            std::cerr << "----- CLbmSolverCPU<T>::setVelocities() -----" << std::endl;
            std::cerr << "There is no open file to write logs." << std::endl;
            std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
            std::cerr << "---------------------------------------------" << std::endl;

            exit (EXIT_FAILURE);
        }
    }

    CVector<3, int> srcOrigin(0);

    for (int i = 0; i < 3; i++)
    {
        setVariable(size, srcOrigin, size, srcArch, origin, &src[i * size.elements()], &velocities[i * domain.getNumOfCellsWithHalo()], NULL);
    }

    if (configuration->doLogging)
    {
        std::stringstream loggingFileName;
        loggingFileName << configuration->loggingOutputDir << "/log_" << id << ".txt";
        std::ofstream loggingFile(loggingFileName.str().c_str(), std::ios::out | std::ios::app);
        if (loggingFile.is_open())
        {
            loggingFile << "A copy operation from " << ((srcArch == CPU) ? "host" : "device") << " to host was performed." << std::endl;
            loggingFile << "---------------------------------------------" << std::endl;
            loggingFile.close();
        } else {
            std::cerr << "----- CLbmSolverCPU<T>::setVelocities() -----" << std::endl;
            std::cerr << "There is no open file to write logs." << std::endl;
            std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
            std::cerr << "---------------------------------------------" << std::endl;

            exit (EXIT_FAILURE);
        }
    }
}

template <class T>
void CLbmSolverCPU<T>::setVelocities(Architecture srcArch, T* src)
{
    CVector<3, int> origin(1);
    CVector<3, int> size(domain.getSize());

    setVelocities(origin, size, srcArch, src);
}

template <class T>
void CLbmSolverCPU<T>::getDensities(CVector<3, int>& origin, CVector<3, int>& size, Architecture dstArch, T* dst)
{
    if (configuration->doLogging)
    {
        std::stringstream loggingFileName;
        loggingFileName << configuration->loggingOutputDir << "/log_" << id << ".txt";
        std::ofstream loggingFile(loggingFileName.str().c_str(), std::ios::out | std::ios::app);
        if (loggingFile.is_open())
        {
            loggingFile << "----- CLbmSolverCPU<T>::getDensities() -----" << std::endl;
            loggingFile << "id:                " << id << std::endl;
            loggingFile << "--------------------------------------------" << std::endl;
            loggingFile << "domain origin:     " << domain.getOrigin() << std::endl;
            loggingFile << "domain size:       " << domain.getSize() << std::endl;
            loggingFile << "--------------------------------------------" << std::endl;
            loggingFile << "cuboid origin:     " << origin << std::endl;
            loggingFile << "cuboid size:       " << size << std::endl;
            loggingFile << "--------------------------------------------" << std::endl;
            loggingFile.close();
        } else {
            std::cerr << "----- CLbmSolverCPU<T>::getDensities() -----" << std::endl;
            std::cerr << "There is no open file to write logs." << std::endl;
            std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
            std::cerr << "--------------------------------------------" << std::endl;

            exit (EXIT_FAILURE);
        }
    }

    CVector<3, int> dstOrigin(0);

    getVariable(size, origin, dstOrigin, size, dstArch, densities, dst, NULL);

    if (configuration->doLogging)
    {
        std::stringstream loggingFileName;
        loggingFileName << configuration->loggingOutputDir << "/log_" << id << ".txt";
        std::ofstream loggingFile(loggingFileName.str().c_str(), std::ios::out | std::ios::app);
        if (loggingFile.is_open())
        {
            loggingFile << "A copy operation from host to " << ((dstArch == CPU) ? "host" : "device") << " was performed." << std::endl;
            loggingFile << "--------------------------------------------" << std::endl;
            loggingFile.close();
        } else {
            std::cerr << "----- CLbmSolverCPU<T>::getDensities() -----" << std::endl;
            std::cerr << "There is no open file to write logs." << std::endl;
            std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
            std::cerr << "--------------------------------------------" << std::endl;

            exit (EXIT_FAILURE);
        }
    }
}

template <class T>
void CLbmSolverCPU<T>::getDensities(Architecture dstArch, T* dst)
{
    CVector<3, int> origin(1);
    CVector<3, int> size(domain.getSize());

    getDensities(origin, size, dstArch, dst);
}

template <class T>
void CLbmSolverCPU<T>::setDensities(CVector<3, int>& origin, CVector<3, int>& size, Architecture srcArch, T* src)
{
    if (configuration->doLogging)
    {
        std::stringstream loggingFileName;
        loggingFileName << configuration->loggingOutputDir << "/log_" << id << ".txt";
        std::ofstream loggingFile(loggingFileName.str().c_str(), std::ios::out | std::ios::app);
        if (loggingFile.is_open())
        {
            loggingFile << "----- CLbmSolverCPU<T>::setDensities() -----" << std::endl;
            loggingFile << "id:                " << id << std::endl;
            loggingFile << "--------------------------------------------" << std::endl;
            loggingFile << "domain origin:     " << domain.getOrigin() << std::endl;
            loggingFile << "domain size:       " << domain.getSize() << std::endl;
            loggingFile << "--------------------------------------------" << std::endl;
            loggingFile << "cuboid origin:     " << origin << std::endl;
            loggingFile << "cuboid size:       " << size << std::endl;
            loggingFile << "--------------------------------------------" << std::endl;
            loggingFile.close();
        } else {
            std::cerr << "----- CLbmSolverCPU<T>::setDensities() -----" << std::endl;
            std::cerr << "There is no open file to write logs." << std::endl;
            std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
            std::cerr << "--------------------------------------------" << std::endl;

            exit (EXIT_FAILURE);
        }
    }

    CVector<3, int> srcOrigin(0);

    setVariable(size, srcOrigin, size, srcArch, origin, src, densities, NULL);

    if (configuration->doLogging)
    {
        std::stringstream loggingFileName;
        loggingFileName << configuration->loggingOutputDir << "/log_" << id << ".txt";
        std::ofstream loggingFile(loggingFileName.str().c_str(), std::ios::out | std::ios::app);
        if (loggingFile.is_open())
        {
            loggingFile << "A copy operation from " << ((srcArch == CPU) ? "host" : "device") << " to host was performed." << std::endl;
            loggingFile << "--------------------------------------------" << std::endl;
            loggingFile.close();
        } else {
            std::cerr << "----- CLbmSolverCPU<T>::setDensities() -----" << std::endl;
            std::cerr << "There is no open file to write logs." << std::endl;
            std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
            std::cerr << "--------------------------------------------" << std::endl;

            exit (EXIT_FAILURE);
        }
    }
}

template <class T>
void CLbmSolverCPU<T>::setDensities(Architecture srcArch, T* src)
{
    CVector<3, int> origin(1);
    CVector<3, int> size(domain.getSize());

    setDensities(origin, size, srcArch, src);
}

template class CLbmSolverCPU<float>;
template class CLbmSolverCPU<double>;
