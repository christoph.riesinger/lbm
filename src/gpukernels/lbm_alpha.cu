/*
 * Copyright
 * 2010 Martin Schreiber
 * 2013 Arash Bakhtiari
 * 2016 Christoph Riesinger, Ayman Saleem
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "lbm_alpha.cuh"

template<typename T>
__global__ void lbm_kernel_alpha(
        T* densitiesDistributions,
        const Flag* flags,
        T* velocities,
        T* densities,
        const T tauInv,
        const T gravitationX,
        const T gravitationY,
        const T gravitationZ,
        const T drivenCavityVelocity,
        const int originX,
        const int originY,
        const int originZ,
        const int sizeX,
        const int sizeY,
        const int sizeZ,
        const int domainCellsX,
        const int domainCellsY,
        const int domainCellsZ,
        const bool storeDensities,
        const bool storeVelocities)
{
    const unsigned int numOfCells = domainCellsX * domainCellsY * domainCellsZ;
    const unsigned int X = blockIdx.x * blockDim.x + threadIdx.x;
    const unsigned int Y = blockIdx.y * blockDim.y + threadIdx.y;
    const unsigned int Z = blockIdx.z * blockDim.z + threadIdx.z;
    const unsigned int id = (originZ + Z) * (domainCellsX * domainCellsY) + (originY + Y) * domainCellsX + (originX + X);

    if (id >= numOfCells)
        return;

    if (X >= sizeX || Y >= sizeY || Z >= sizeZ)
        return;

    const Flag flag = flags[id];

    if  (flag == GHOST_LAYER)
        return;

    T velX, velY, velZ;
    T dd0, dd1, dd2, dd3, dd4, dd5, dd6, dd7, dd8, dd9, dd10, dd11, dd12, dd13, dd14, dd15, dd16, dd17, dd18;
    T rho;
    T velSqr, velComb, velCombSqr;
    T dd_param, tmp;
    T *current_dds = &densitiesDistributions[id];

    /*
     * +++++++++++
     * +++ DD0 +++
     * +++++++++++
     *
     * 0-3: f(1,0,0), f(-1,0,0),  f(0,1,0), f(0,-1,0)
     */
    dd0 = *current_dds;
    current_dds += numOfCells;
    rho = dd0;
    velX = dd0;
    dd1 = *current_dds;
    current_dds += numOfCells;
    rho += dd1;
    velX -= dd1;
    dd2 = *current_dds;
    current_dds += numOfCells;
    rho += dd2;
    velY = dd2;
    dd3 = *current_dds;
    current_dds += numOfCells;
    rho += dd3;
    velY -= dd3;

    /*
     * +++++++++++
     * +++ DD1 +++
     * +++++++++++
     *
     * 4-7: f(1,1,0), f(-1,-1,0), f(1,-1,0), f(-1,1,0)
     */
    dd4 = *current_dds;
    current_dds += numOfCells;
    rho += dd4;
    velX += dd4;
    velY += dd4;
    dd5 = *current_dds;
    current_dds += numOfCells;
    rho += dd5;
    velX -= dd5;
    velY -= dd5;
    dd6 = *current_dds;
    current_dds += numOfCells;
    rho += dd6;
    velX += dd6;
    velY -= dd6;
    dd7 = *current_dds;
    current_dds += numOfCells;
    rho += dd7;
    velX -= dd7;
    velY += dd7;

    /*
     * +++++++++++
     * +++ DD2 +++
     * +++++++++++
     *
     * 8-11: f(1,0,1), f(-1,0,-1), f(1,0,-1), f(-1,0,1)
     */
    dd8 = *current_dds;
    current_dds += numOfCells;
    rho += dd8;
    velX += dd8;
    velZ = dd8;
    dd9 = *current_dds;
    current_dds += numOfCells;
    rho += dd9;
    velX -= dd9;
    velZ -= dd9;
    dd10 = *current_dds;
    current_dds += numOfCells;
    rho += dd10;
    velX += dd10;
    velZ -= dd10;
    dd11 = *current_dds;
    current_dds += numOfCells;
    rho += dd11;
    velX -= dd11;
    velZ += dd11;

    /*
     * +++++++++++
     * +++ DD3 +++
     * +++++++++++
     *
     * 12-15: f(0,1,1), f(0,-1,-1), f(0,1,-1), f(0,-1,1)
     */
    dd12 = *current_dds;
    current_dds += numOfCells;
    rho += dd12;
    velY += dd12;
    velZ += dd12;
    dd13 = *current_dds;
    current_dds += numOfCells;
    rho += dd13;
    velY -= dd13;
    velZ -= dd13;
    dd14 = *current_dds;
    current_dds += numOfCells;
    rho += dd14;
    velY += dd14;
    velZ -= dd14;
    dd15 = *current_dds;
    current_dds += numOfCells;
    rho += dd15;
    velY -= dd15;
    velZ += dd15;

    /*
     * +++++++++++
     * +++ DD4 +++
     * +++++++++++
     *
     * 16-18: f(0,0,1), f(0,0,-1), f(0,0,0)
     */
    dd16 = *current_dds;
    current_dds += numOfCells;
    rho += dd16;
    velZ += dd16;
    dd17 = *current_dds;
    current_dds += numOfCells;
    rho += dd17;
    velZ -= dd17;
    dd18 = *current_dds;
    rho += dd18;

    current_dds = &densitiesDistributions[id];

    switch(flag)
    {
        case (FLUID):
            dd_param = rho - ((T)3/(T)2)* (velX * velX + velY * velY + velZ * velZ);

            /*
             * +++++++++++
             * +++ DD0 +++
             * +++++++++++
             */
            tmp = gravitationX * ((T)1/(T)18) * rho;
            velSqr = velX * velX;
            dd1 += tauInv * (eq_dd_a1(velX, velSqr, dd_param) - dd1);
            dd1 -= tmp;
            *current_dds = dd1;
            current_dds += numOfCells;
            dd0 += tauInv * (eq_dd_a0(velX, velSqr, dd_param) - dd0);
            dd0 += tmp;
            *current_dds = dd0;
            current_dds += numOfCells;
            tmp = gravitationY * ((T)-1/(T)18) * rho;
            velSqr = velY * velY;
            dd3 += tauInv * (eq_dd_a1(velY, velSqr, dd_param) - dd3);
            dd3 -= tmp;
            *current_dds = dd3;
            current_dds += numOfCells;
            dd2 += tauInv * (eq_dd_a0(velY, velSqr, dd_param) - dd2);
            dd2 += tmp;
            *current_dds = dd2;
            current_dds += numOfCells;

            /*
             * +++++++++++
             * +++ DD1 +++
             * +++++++++++
             */
            velComb = velX + velY;
            velCombSqr = velComb * velComb;
            tmp = (gravitationX - gravitationY) * ((T)1/(T)36) * rho;
            dd5 += tauInv*(eq_dd5(velComb, velCombSqr, dd_param) - dd5);
            dd5 -= tmp;
            *current_dds = dd5;
            current_dds += numOfCells;
            dd4 += tauInv*(eq_dd4(velComb, velCombSqr, dd_param) - dd4);
            dd4 += tmp;
            *current_dds = dd4;
            current_dds += numOfCells;
            velComb = velX - velY;
            velCombSqr = velComb * velComb;
            tmp = (gravitationX + gravitationY) * ((T)1/(T)36) * rho;
            dd7 += tauInv * (eq_dd5(velComb, velCombSqr, dd_param) - dd7);
            dd7 -= tmp;
            *current_dds = dd7;
            current_dds += numOfCells;
            dd6 += tauInv * (eq_dd4(velComb, velCombSqr, dd_param) - dd6);
            dd6 += tmp;
            *current_dds = dd6;
            current_dds += numOfCells;

            /*
             * +++++++++++
             * +++ DD2 +++
             * +++++++++++
             */
            velComb = velX+velZ;
            velCombSqr = velComb * velComb;
            tmp = (gravitationX + gravitationZ) * ((T)1/(T)36) * rho;
            dd9 += tauInv * (eq_dd5(velComb, velCombSqr, dd_param) - dd9);
            dd9 -= tmp;
            *current_dds = dd9;
            current_dds += numOfCells;
            dd8 += tauInv * (eq_dd4(velComb, velCombSqr, dd_param) - dd8);
            dd8 += tmp;
            *current_dds = dd8;
            current_dds += numOfCells;
            tmp = (gravitationX - gravitationZ) * ((T)1/(T)36) * rho;
            velComb = velX - velZ;
            velCombSqr = velComb * velComb;
            dd11 += tauInv * (eq_dd5(velComb, velCombSqr, dd_param) - dd11);
            dd11 -= tmp;
            *current_dds = dd11;
            current_dds += numOfCells;
            dd10 += tauInv * (eq_dd4(velComb, velCombSqr, dd_param) - dd10);
            dd10 += tmp;
            *current_dds = dd10;
            current_dds += numOfCells;

            /*
             * +++++++++++
             * +++ DD3 +++
             * +++++++++++
             */
            velComb = velY + velZ;
            velCombSqr = velComb * velComb;
            tmp = (gravitationZ - gravitationY) * ((T)1/(T)36) * rho;
            dd13 += tauInv * (eq_dd5(velComb, velCombSqr, dd_param) - dd13);
            dd13 -= tmp;
            *current_dds = dd13;
            current_dds += numOfCells;
            dd12 += tauInv * (eq_dd4(velComb, velCombSqr, dd_param) - dd12);
            dd12 += tmp;
            *current_dds = dd12;
            current_dds += numOfCells;
            velComb = velY - velZ;
            velCombSqr = velComb * velComb;
            tmp = (gravitationZ + gravitationY)*((T)-1/(T)36)*rho;
            dd15 += tauInv * (eq_dd5(velComb, velCombSqr, dd_param) - dd15);
            dd15 -= tmp;
            *current_dds = dd15;
            current_dds += numOfCells;
            dd14 += tauInv * (eq_dd4(velComb, velCombSqr, dd_param) - dd14);
            dd14 += tmp;
            *current_dds = dd14;
            current_dds += numOfCells;

            /*
             * +++++++++++
             * +++ DD4 +++
             * +++++++++++
             */
            velSqr = velZ * velZ;
            tmp = gravitationZ * ((T)1/(T)18) * rho;
            dd17 += tauInv * (eq_dd_a1(velZ, velSqr, dd_param) - dd17);
            dd17 -= tmp;
            *current_dds = dd17;
            current_dds += numOfCells;
            dd16 += tauInv * (eq_dd_a0(velZ, velSqr, dd_param) - dd16);
            dd16 += tmp;
            *current_dds = dd16;
            current_dds += numOfCells;
            dd18 += tauInv * (eq_dd18(dd_param) - dd18);
            *current_dds = dd18;

            break;
        case (OBSTACLE):
            if (storeVelocities)
            {
                velX = (T)0;
                velY = (T)0;
                velZ = (T)0;
            }

            break;
        case (VELOCITY_INJECTION):
            velX = drivenCavityVelocity;
            velY = (T)0;
            velZ = (T)0;
            rho = (T)1;
            dd_param = rho - ((T)3/(T)2) * (velX * velX + velY * velY + velZ * velZ);

            /*
             * +++++++++++
             * +++ DD0 +++
             * +++++++++++
             */
            velSqr = velX * velX;
            tmp = gravitationX * ((T)1/(T)18) * rho;
            dd1 = eq_dd_a1(velX, velSqr, dd_param);
            dd1 -= tmp;
            *current_dds = dd1;
            current_dds += numOfCells;
            dd0 = eq_dd_a0(velX, velSqr, dd_param);
            dd0 += tmp;
            *current_dds = dd0;
            current_dds += numOfCells;
            velSqr = velY * velY;
            tmp = gravitationY * ((T)-1/(T)18) * rho;
            dd3 = eq_dd_a1(velY, velSqr, dd_param);
            dd3 -= tmp;
            *current_dds = dd3;
            current_dds += numOfCells;
            dd2 = eq_dd_a0(velY, velSqr, dd_param);
            dd2 += tmp;
            *current_dds = dd2;
            current_dds += numOfCells;

            /*
             * +++++++++++
             * +++ DD1 +++
             * +++++++++++
             */
            velComb = velX + velY;
            velCombSqr = velComb * velComb;
            tmp = (gravitationX - gravitationY) * ((T)1/(T)36) * rho;
            dd5 = eq_dd5(velComb, velCombSqr, dd_param);
            dd5 -= tmp;
            *current_dds = dd5;
            current_dds += numOfCells;
            dd4 = eq_dd4(velComb, velCombSqr, dd_param);
            dd4 += tmp;
            *current_dds = dd4;
            current_dds += numOfCells;
            velComb = velX - velY;
            velCombSqr = velComb * velComb;
            tmp = (gravitationX + gravitationY) * ((T)1/(T)36) * rho;
            dd7 = eq_dd5(velComb, velCombSqr, dd_param);
            dd7 -= tmp;
            *current_dds = dd7;
            current_dds += numOfCells;
            dd6 = eq_dd4(velComb, velCombSqr, dd_param);
            dd6 += tmp;
            *current_dds = dd6;
            current_dds += numOfCells;

            /*
             * +++++++++++
             * +++ DD2 +++
             * +++++++++++
             */
            velComb = velX + velZ;
            velCombSqr = velComb * velComb;
            tmp = (gravitationX + gravitationZ) * ((T)1/(T)36) * rho;
            dd9 = eq_dd5(velComb, velCombSqr, dd_param);
            dd9 -= tmp;
            *current_dds = dd9;
            current_dds += numOfCells;
            dd8 = eq_dd4(velComb, velCombSqr, dd_param);
            dd8 += tmp;
            *current_dds = dd8;
            current_dds += numOfCells;
            velComb = velX - velZ;
            velCombSqr = velComb * velComb;
            tmp = (gravitationX - gravitationZ) * ((T)1/(T)36) * rho;
            dd11 = eq_dd5(velComb, velCombSqr, dd_param);
            dd11 -= tmp;
            *current_dds = dd11;
            current_dds += numOfCells;
            dd10 = eq_dd4(velComb, velCombSqr, dd_param);
            dd10 += tmp;
            *current_dds = dd10;
            current_dds += numOfCells;

            /*
             * +++++++++++
             * +++ DD3 +++
             * +++++++++++
             */
            velComb = velY + velZ;
            velCombSqr = velComb * velComb;
            tmp = (gravitationZ - gravitationY) * ((T)1/(T)36) * rho;
            dd13 = eq_dd5(velComb, velCombSqr, dd_param);
            dd13 -= tmp;
            *current_dds = dd13;
            current_dds += numOfCells;
            dd12 = eq_dd4(velComb, velCombSqr, dd_param);
            dd12 += tmp;
            *current_dds = dd12;
            current_dds += numOfCells;
            velComb = velY - velZ;
            velCombSqr = velComb * velComb;
            tmp = (gravitationZ + gravitationY) * ((T)-1/(T)36) * rho;
            dd15 = eq_dd5(velComb, velCombSqr, dd_param);
            dd15 -= tmp;
            *current_dds = dd15;
            current_dds += numOfCells;
            dd14 = eq_dd4(velComb, velCombSqr, dd_param);
            dd14 += tmp;
            *current_dds = dd14;
            current_dds += numOfCells;

            /*
             * +++++++++++
             * +++ DD4 +++
             * +++++++++++
             */
            velSqr = velZ * velZ;
            tmp = gravitationZ * ((T)1/(T)18) * rho;
            dd17 = eq_dd_a1(velZ, velSqr, dd_param);
            dd17 -= tmp;
            *current_dds = dd17;
            current_dds += numOfCells;
            dd16 = eq_dd_a0(velZ, velSqr, dd_param);
            dd16 += tmp;
            *current_dds = dd16;
            current_dds += numOfCells;
            dd18 = eq_dd18(dd_param);
            *current_dds = dd18;

            break;
        case (GHOST_LAYER):
            break;
    }

    if (storeVelocities)
    {
        current_dds = &velocities[id];
        *current_dds += (T)1;
        current_dds += numOfCells;
        *current_dds += (T)1;
        current_dds += numOfCells;
        *current_dds += (T)1;
    }

    if (storeDensities)
        densities[id] = rho;
}

template __global__ void lbm_kernel_alpha<float>(
        float *densitiesDistributions,
        const Flag *flags,
        float *velocities,
        float *densities,
        const float tauInv,
        const float gravitationX,
        const float gravitationY,
        const float gravitationZ,
        const float drivenCavityVelocity,
        const int originX,
        const int originY,
        const int originZ,
        const int sizeX,
        const int sizeY,
        const int sizeZ,
        const int domainCellsX,
        const int domainCellsY,
        const int domainCellsZ,
        const bool storeDensities,
        const bool storeVelocities);
template __global__ void lbm_kernel_alpha<double>(
        double *densitiesDistributions,
        const Flag *flags,
        double *velocities,
        double *densities,
        const double tauInv,
        const double gravitationX,
        const double gravitationY,
        const double gravitationZ,
        const double drivenCavityVelocity,
        const int originX,
        const int originY,
        const int originZ,
        const int sizeX,
        const int sizeY,
        const int sizeZ,
        const int domainCellsX,
        const int domainCellsY,
        const int domainCellsZ,
        const bool storeDensities,
        const bool storeVelocities);
