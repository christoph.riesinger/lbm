/*
 * Copyright
 * 2010 Martin Schreiber
 * 2013 Arash Bakhtiari
 * 2016 Christoph Riesinger, Ayman Saleem
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "CController.hpp"

#include <cassert>
#include <fstream>
#include <sstream>
#include <typeinfo>
#include <sys/time.h>

#ifdef USE_MPI
#include <mpi.h>
#endif

#include "libvis/CLbmVisualizationNetCDF.hpp"

template <class T>
CController<T>::CController(
        int id,
        CDomain<T> domain,
        std::vector<Flag> boundaryConditions,
        std::vector<CComm<T> > communication,
        CConfiguration<T>* configuration) :
        id(id),
        domain(domain),
        boundaryConditions(boundaryConditions),
        communication(communication),
        configuration(configuration),
        simulationStepCounter(0)
{
    CVector<3, int> originCPU = domain.getOrigin();
    CVector<3, int> sizeCPU = domain.getSize();
    sizeCPU[1] *= configuration->CPUSubdomainRatio;
    CVector<3, int> originGPU = domain.getOrigin();
    originGPU[1] += sizeCPU[1];
    CVector<3, int> sizeGPU = domain.getSize();
    sizeGPU[1] -= sizeCPU[1];
    CVector<3, T> lengthCPU = domain.getLength();
    lengthCPU[1] = domain.getLength()[1] * (T)sizeCPU[1] / (T)domain.getSize()[1];
    CVector<3, T> lengthGPU = domain.getLength();
    lengthGPU[1] -= lengthCPU[1];

    CDomain<T> domainCPU(id, sizeCPU, originCPU, lengthCPU);
    CDomain<T> domainGPU(id, sizeGPU, originGPU, lengthGPU);

    if (configuration->doLogging)
    {
        std::stringstream loggingFileName;
        loggingFileName << configuration->loggingOutputDir << "/log_" << id << ".txt";
        std::ofstream loggingFile(loggingFileName.str().c_str(), std::ios::out | std::ios::app);
        if (loggingFile.is_open())
        {
            loggingFile << "----- CController<T>::CController() -----" << std::endl;
            loggingFile << "CPU subdomain size:   " << domainCPU.getSize() << std::endl;
            loggingFile << "CPU subdomain length: " << domainCPU.getLength() << std::endl;
            loggingFile << "CPU subdomain origin: " << domainCPU.getOrigin() << std::endl;
            loggingFile << "-----------------------------------------" << std::endl;
            loggingFile << "GPU subdomain size:   " << domainGPU.getSize() << std::endl;
            loggingFile << "GPU subdomain length: " << domainGPU.getLength() << std::endl;
            loggingFile << "GPU subdomain origin: " << domainGPU.getOrigin() << std::endl;
            loggingFile << "-----------------------------------------" << std::endl;
            loggingFile.close();
        } else {
            std::cerr << "----- CController<T>::CController() -----" << std::endl;
            std::cerr << "There is no open file to write logs." << std::endl;
            std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
            std::cerr << "-----------------------------------------" << std::endl;

            exit (EXIT_FAILURE);
        }
    }

    assert(domainCPU.hasDomain() || domainGPU.hasDomain());

    std::vector<Flag> boundaryConditionsCPU = this->boundaryConditions;
    std::vector<Flag> boundaryConditionsGPU = this->boundaryConditions;

    if (domainCPU.hasDomain())
        boundaryConditionsGPU[2] = GHOST_LAYER;
    if (domainGPU.hasDomain())
        boundaryConditionsCPU[3] = GHOST_LAYER;

    solverCPU = new CLbmSolverCPU<T>(
            this->id,
            domainCPU,
            boundaryConditionsCPU,
            this->configuration);
    solverGPU = new CLbmSolverGPU<T>(
            this->id,
            domainGPU,
            boundaryConditionsGPU,
            this->configuration);

    boundaryOriginsCPU = new std::vector<CVector<3, int> >();
    boundarySizesCPU = new std::vector<CVector<3, int> >();
    boundaryOriginsGPU = new std::vector<CVector<3, int> >();
    boundarySizesGPU = new std::vector<CVector<3, int> >();
    innerOriginCPU.setZero();
    innerSizeCPU = domainCPU.getSizeWithHalo();
    innerOriginGPU.setZero();
    innerSizeGPU = domainGPU.getSizeWithHalo();
    /*
     * Insert boundary layers of interface between CPU and GPU subdomain.
     */
    if (solverCPU->getDomain()->hasDomain() && solverGPU->getDomain()->hasDomain())
    {
        CVector<3, int> boundaryOriginCPU(innerOriginCPU);
        boundaryOriginCPU[1] = domainCPU.getSizeWithHalo()[1] - 2;
        CVector<3, int> boundarySizeCPU(innerSizeCPU[0], 2, innerSizeCPU[2]);
        boundaryOriginsCPU->push_back(boundaryOriginCPU);
        boundarySizesCPU->push_back(boundarySizeCPU);
        innerSizeCPU[1] -= 2;

        CVector<3, int> boundaryOriginGPU(innerOriginGPU);
        CVector<3, int> boundarySizeGPU(innerSizeGPU[0], 2, innerSizeGPU[2]);
        boundaryOriginsGPU->push_back(boundaryOriginGPU);
        boundarySizesGPU->push_back(boundarySizeGPU);
        innerOriginGPU[1] += 2;
        innerSizeGPU[1] -= 2;
    }
    /*
     * Insert boundary layers to neighboring subdomains.
     */
    for (unsigned int i = 0; i < communication.size(); i++)
    {
        switch(communication[i].getDirection())
        {
        case LEFT:
            if (solverCPU->getDomain()->hasDomain())
            {
                CVector<3, int> boundaryOrigin(innerOriginCPU);
                CVector<3, int> boundarySize(2, innerSizeCPU[1], innerSizeCPU[2]);
                boundaryOriginsCPU->push_back(boundaryOrigin);
                boundarySizesCPU->push_back(boundarySize);
                innerOriginCPU[0] += 2;
                innerSizeCPU[0] -= 2;
            }
            if (solverGPU->getDomain()->hasDomain())
            {
                CVector<3, int> boundaryOrigin(innerOriginGPU);
                CVector<3, int> boundarySize(2, innerSizeGPU[1], innerSizeGPU[2]);
                boundaryOriginsGPU->push_back(boundaryOrigin);
                boundarySizesGPU->push_back(boundarySize);
                innerOriginGPU[0] += 2;
                innerSizeGPU[0] -= 2;
            }
            break;
        case RIGHT:
            if (solverCPU->getDomain()->hasDomain())
            {
                CVector<3, int> boundaryOrigin(innerOriginCPU);
                boundaryOrigin[0] = domainCPU.getSizeWithHalo()[0] - 2;
                CVector<3, int> boundarySize(2, innerSizeCPU[1], innerSizeCPU[2]);
                boundaryOriginsCPU->push_back(boundaryOrigin);
                boundarySizesCPU->push_back(boundarySize);
                innerSizeCPU[0] -= 2;
            }
            if (solverGPU->getDomain()->hasDomain())
            {
                CVector<3, int> boundaryOrigin(innerOriginGPU);
                boundaryOrigin[0] = domainGPU.getSizeWithHalo()[0] - 2;
                CVector<3, int> boundarySize(2, innerSizeGPU[1], innerSizeGPU[2]);
                boundaryOriginsGPU->push_back(boundaryOrigin);
                boundarySizesGPU->push_back(boundarySize);
                innerSizeGPU[0] -= 2;
            }
            break;
        case BOTTOM:
            if (solverCPU->getDomain()->hasDomain())
            {
                CVector<3, int> boundaryOrigin(innerOriginCPU);
                CVector<3, int> boundarySize(innerSizeCPU[0], 2, innerSizeCPU[2]);
                boundaryOriginsCPU->push_back(boundaryOrigin);
                boundarySizesCPU->push_back(boundarySize);
                innerOriginCPU[1] += 2;
                innerSizeCPU[1] -= 2;
            } else {
                CVector<3, int> boundaryOrigin(innerOriginGPU);
                CVector<3, int> boundarySize(innerSizeGPU[0], 2, innerSizeGPU[2]);
                boundaryOriginsGPU->push_back(boundaryOrigin);
                boundarySizesGPU->push_back(boundarySize);
                innerOriginGPU[1] += 2;
                innerSizeGPU[1] -= 2;
            }
            break;
        case TOP:
            if (solverGPU->getDomain()->hasDomain())
            {
                CVector<3, int> boundaryOrigin(innerOriginGPU);
                boundaryOrigin[1] = domainGPU.getSizeWithHalo()[1] - 2;
                CVector<3, int> boundarySize(innerSizeGPU[0], 2, innerSizeGPU[2]);
                boundaryOriginsGPU->push_back(boundaryOrigin);
                boundarySizesGPU->push_back(boundarySize);
                innerSizeGPU[1] -= 2;
            } else {
                CVector<3, int> boundaryOrigin(innerOriginCPU);
                boundaryOrigin[1] = domainCPU.getSizeWithHalo()[1] - 2;
                CVector<3, int> boundarySize(innerSizeCPU[0], 2, innerSizeCPU[2]);
                boundaryOriginsCPU->push_back(boundaryOrigin);
                boundarySizesCPU->push_back(boundarySize);
                innerSizeCPU[1] -= 2;
            }
            break;
        case BACK:
            if (solverCPU->getDomain()->hasDomain())
            {
                CVector<3, int> boundaryOrigin(innerOriginCPU);
                CVector<3, int> boundarySize(innerSizeCPU[0], innerSizeCPU[1], 2);
                boundaryOriginsCPU->push_back(boundaryOrigin);
                boundarySizesCPU->push_back(boundarySize);
                innerOriginCPU[2] += 2;
                innerSizeCPU[2] -= 2;
            }
            if (solverGPU->getDomain()->hasDomain())
            {
                CVector<3, int> boundaryOriginGPU(innerOriginGPU);
                CVector<3, int> boundarySizeGPU(innerSizeGPU[0], innerSizeGPU[1], 2);
                boundaryOriginsGPU->push_back(boundaryOriginGPU);
                boundarySizesGPU->push_back(boundarySizeGPU);
                innerOriginGPU[2] += 2;
                innerSizeGPU[2] -= 2;
            }
            break;
        case FRONT:
            if (solverCPU->getDomain()->hasDomain())
            {
                CVector<3, int> boundaryOrigin(innerOriginCPU);
                boundaryOrigin[2] = domainCPU.getSizeWithHalo()[2] - 2;
                CVector<3, int> boundarySize(innerSizeCPU[0], innerSizeCPU[1], 2);
                boundaryOriginsCPU->push_back(boundaryOrigin);
                boundarySizesCPU->push_back(boundarySize);
                innerSizeCPU[2] -= 2;
            }
            if (solverGPU->getDomain()->hasDomain())
            {
                CVector<3, int> boundaryOrigin(innerOriginGPU);
                boundaryOrigin[2] = domainGPU.getSizeWithHalo()[2] - 2;
                CVector<3, int> boundarySize(innerSizeGPU[0], innerSizeGPU[1], 2);
                boundaryOriginsGPU->push_back(boundaryOrigin);
                boundarySizesGPU->push_back(boundarySize);
                innerSizeGPU[2] -= 2;
            }
            break;
        }
    }

#ifdef USE_MPI
    sendBuffers = new std::vector<T*>(this->communication.size());
    recvBuffers = new std::vector<T*>(this->communication.size());
    sendRequests = new MPI_Request[this->communication.size()];
    recvRequests = new MPI_Request[this->communication.size()];
    for (unsigned int i = 0; i < this->communication.size(); i++)
    {
        sendBuffers->at(i) = new T[NUM_LATTICE_VECTORS * this->communication[i].getSendSize().elements()];
        recvBuffers->at(i) = new T[NUM_LATTICE_VECTORS * this->communication[i].getRecvSize().elements()];
    }
#endif

    streamsCPU = new std::vector<cudaStream_t>(std::max(boundaryOriginsCPU->size(), boundaryOriginsGPU->size()));
    streamsGPU = new std::vector<cudaStream_t>(std::max(boundaryOriginsCPU->size(), boundaryOriginsGPU->size()));
    for (unsigned int i = 0; i < streamsCPU->size(); i++)
        GPU_ERROR_CHECK(cudaStreamCreate(&streamsCPU->at(i)))
    for (unsigned int i = 0; i < streamsGPU->size(); i++)
        GPU_ERROR_CHECK(cudaStreamCreate(&streamsGPU->at(i)))
    GPU_ERROR_CHECK(cudaStreamCreate(&innerStream))
    GPU_ERROR_CHECK(cudaStreamCreate(&CPUtoGPUStream))
    GPU_ERROR_CHECK(cudaStreamCreate(&GPUtoCPUStream))

    if (this->configuration->doVisualization)
        visualization = new CLbmVisualizationNetCDF<T>(
                id,
                this->configuration->visualizationRate,
                solverCPU,
                solverGPU,
#if defined(USE_MPI) && defined(PAR_NETCDF)
                this->configuration->numOfSubdomains,
#endif
                this->configuration->visualizationOutputDir);
}

template <class T>
CController<T>::~CController()
{
    if (configuration->doVisualization)
        delete visualization;

    GPU_ERROR_CHECK(cudaStreamDestroy(GPUtoCPUStream))
    GPU_ERROR_CHECK(cudaStreamDestroy(CPUtoGPUStream))
    GPU_ERROR_CHECK(cudaStreamDestroy(innerStream))
    delete streamsGPU;
    delete streamsCPU;

#ifdef USE_MPI
    delete[] recvRequests;
    delete[] sendRequests;
    delete recvBuffers;
    delete sendBuffers;
#endif

    delete boundarySizesGPU;
    delete boundaryOriginsGPU;
    delete boundarySizesCPU;
    delete boundaryOriginsCPU;

    delete solverCPU;
    delete solverGPU;
}

template <class T>
void CController<T>::stepAlpha()
{
    if (configuration->doLogging)
    {
        std::stringstream loggingFileName;
        loggingFileName << configuration->loggingOutputDir << "/log_" << id << ".txt";
        std::ofstream loggingFile(loggingFileName.str().c_str(), std::ios::out | std::ios::app);
        if (loggingFile.is_open())
        {
            loggingFile << "----- CController<T>::stepAlpha() -----" << std::endl;
            loggingFile.close();
        } else {
            std::cerr << "----- CController<T>::stepAlpha() -----" << std::endl;
            std::cerr << "There is no open file to write logs." << std::endl;
            std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
            std::cerr << "---------------------------------------" << std::endl;

            exit (EXIT_FAILURE);
        }
    }

#ifdef USE_MPI
    /*
     * Post receive statements for boundary regions of neighboring subdomains.
     */
    for (unsigned int i = 0; i < communication.size(); i++)
    {
        MPI_Irecv(
                recvBuffers->at(i),
                NUM_LATTICE_VECTORS * communication[i].getRecvSize().elements(),
                ((typeid(T) == typeid(float)) ? MPI_FLOAT : MPI_DOUBLE),
                communication[i].getDstId(),
                simulationStepCounter,
                MPI_COMM_WORLD,
                &recvRequests[i]);
    }
#endif

    /*
     * Compute all boundary regions of the GPU domain.
     */
    for (unsigned int i = 0; i < boundaryOriginsGPU->size(); i++)
    {
        solverGPU->simulationStepAlpha(boundaryOriginsGPU->at(i), boundarySizesGPU->at(i), &streamsGPU->at(i));

        if (configuration->doLogging)
        {
            std::stringstream loggingFileName;
            loggingFileName << configuration->loggingOutputDir << "/log_" << id << ".txt";
            std::ofstream loggingFile(loggingFileName.str().c_str(), std::ios::out | std::ios::app);
            if (loggingFile.is_open())
            {
                loggingFile << "The following GPU boundary was issued to be computed:" << std::endl;
                loggingFile << "boundary origin:        " << boundaryOriginsGPU->at(i) << std::endl;
                loggingFile << "boundary size:          " << boundarySizesGPU->at(i) << std::endl;
                loggingFile << "---------------------------------------" << std::endl;
                loggingFile.close();
            } else {
                std::cerr << "----- CController<T>::stepAlpha() -----" << std::endl;
                std::cerr << "There is no open file to write logs." << std::endl;
                std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
                std::cerr << "---------------------------------------" << std::endl;

                exit (EXIT_FAILURE);
            }
        }
    }

    /*
     * Compute all boundary regions of the CPU domain.
     */
#pragma omp parallel
#pragma omp single
    {
        for (unsigned int i = 0; i < boundaryOriginsCPU->size(); i++)
        {
            solverCPU->simulationStepAlpha(boundaryOriginsCPU->at(i), boundarySizesCPU->at(i));

            if (configuration->doLogging)
            {
                std::stringstream loggingFileName;
                loggingFileName << configuration->loggingOutputDir << "/log_" << id << ".txt";
                std::ofstream loggingFile(loggingFileName.str().c_str(), std::ios::out | std::ios::app);
                if (loggingFile.is_open())
                {
                    loggingFile << "The following CPU boundary was issued to be computed:" << std::endl;
                    loggingFile << "boundary origin:        " << boundaryOriginsCPU->at(i) << std::endl;
                    loggingFile << "boundary size:          " << boundarySizesCPU->at(i) << std::endl;
                    loggingFile << "---------------------------------------" << std::endl;
                    loggingFile.close();
                } else {
                    std::cerr << "----- CController<T>::stepAlpha() -----" << std::endl;
                    std::cerr << "There is no open file to write logs." << std::endl;
                    std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
                    std::cerr << "---------------------------------------" << std::endl;

                    exit (EXIT_FAILURE);
                }
            }
        }
    }
    /*
     * Implicit synchronization point after OpenMP parallel region after all
     * boundary regions of the CPU have been processed.
     */

    /*
     * Explicit synchronization point after all boundary regions of the GPU
     * have been processed.
     */
    GPU_ERROR_CHECK(cudaDeviceSynchronize())

    /*
     * Compute inner region of GPU domain. This call has to be done outside the
     * following parallel region because innerStream is also created outside a
     * parallel region. This launch is asynchronous since a stream is passed.
     */
    solverGPU->simulationStepAlpha(innerOriginGPU, innerSizeGPU, &innerStream);

#pragma omp parallel
#pragma omp single
    {
        /*
         * Compute inner region of CPU domain. This launch is asynchronous since
         * a single master task spawns new tasks.
         */
        solverCPU->simulationStepAlpha(innerOriginCPU, innerSizeCPU);

#ifdef USE_MPI
        int sendIdx, recvIdx;

        sendIdx = recvIdx = 0;

        /*
         * If there is only a CPU or a GPU domain, the send/receive buffers can
         * be handled in a straight forward way. No communication of the local
         * CPU and GPU subdomain is required/possible.
         */
#endif
        if (solverCPU->getDomain()->hasDomain() ^ solverGPU->getDomain()->hasDomain())
        {
#ifdef USE_MPI
            CVector<3, int> sendOrigin, recvOrigin;
            CVector<3, int> sendSize, recvSize;

            if (static_cast<size_t>(sendIdx) < communication.size() && communication[sendIdx].getDirection() == LEFT)
            {
                sendOrigin = communication[sendIdx].getSendOrigin();
                sendSize = communication[sendIdx].getSendSize();

                if (solverCPU->getDomain()->hasDomain())
                {
                    solverCPU->getDensityDistributions(sendOrigin, sendSize, CPU, sendBuffers->at(sendIdx), &streamsCPU->at(sendIdx));
                    GPU_ERROR_CHECK(cudaStreamSynchronize(streamsCPU->at(sendIdx)))
                } else {
                    solverGPU->getDensityDistributions(sendOrigin, sendSize, CPU, sendBuffers->at(sendIdx), &streamsGPU->at(sendIdx));
                    GPU_ERROR_CHECK(cudaStreamSynchronize(streamsGPU->at(sendIdx)))
                }

                MPI_Isend(
                        sendBuffers->at(sendIdx),
                        NUM_LATTICE_VECTORS * communication[sendIdx].getSendSize().elements(),
                        ((typeid(T) == typeid(float)) ? MPI_FLOAT : MPI_DOUBLE),
                        communication[sendIdx].getDstId(),
                        simulationStepCounter,
                        MPI_COMM_WORLD,
                        &sendRequests[sendIdx]);

                sendIdx++;
            }
            if (static_cast<size_t>(sendIdx) < communication.size() && communication[sendIdx].getDirection() == RIGHT)
            {
                sendOrigin = communication[sendIdx].getSendOrigin();
                sendSize = communication[sendIdx].getSendSize();

                if (solverCPU->getDomain()->hasDomain())
                {
                    solverCPU->getDensityDistributions(sendOrigin, sendSize, CPU, sendBuffers->at(sendIdx), &streamsCPU->at(sendIdx));
                    GPU_ERROR_CHECK(cudaStreamSynchronize(streamsCPU->at(sendIdx)))
                } else {
                    solverGPU->getDensityDistributions(sendOrigin, sendSize, CPU, sendBuffers->at(sendIdx), &streamsGPU->at(sendIdx));
                    GPU_ERROR_CHECK(cudaStreamSynchronize(streamsGPU->at(sendIdx)))
                }

                MPI_Isend(
                        sendBuffers->at(sendIdx),
                        NUM_LATTICE_VECTORS * communication[sendIdx].getSendSize().elements(),
                        ((typeid(T) == typeid(float)) ? MPI_FLOAT : MPI_DOUBLE),
                        communication[sendIdx].getDstId(),
                        simulationStepCounter,
                        MPI_COMM_WORLD,
                        &sendRequests[sendIdx]);

                sendIdx++;
            }
            if (static_cast<size_t>(recvIdx) < communication.size() && communication[recvIdx].getDirection() == LEFT)
            {
                recvOrigin = communication[recvIdx].getRecvOrigin();
                recvSize = communication[recvIdx].getRecvSize();

                MPI_Wait(&recvRequests[recvIdx], MPI_STATUS_IGNORE);

                if (solverCPU->getDomain()->hasDomain())
                {
                    solverCPU->setDensityDistributions(recvOrigin, recvSize, CPU, recvBuffers->at(recvIdx), &streamsCPU->at(recvIdx));
                    GPU_ERROR_CHECK(cudaStreamSynchronize(streamsCPU->at(recvIdx)))
                } else {
                    solverGPU->setDensityDistributions(recvOrigin, recvSize, CPU, recvBuffers->at(recvIdx), &streamsGPU->at(recvIdx));
                    GPU_ERROR_CHECK(cudaStreamSynchronize(streamsGPU->at(recvIdx)))
                }

                recvIdx++;
            }
            if (static_cast<size_t>(recvIdx) < communication.size() && communication[recvIdx].getDirection() == RIGHT)
            {
                recvOrigin = communication[recvIdx].getRecvOrigin();
                recvSize = communication[recvIdx].getRecvSize();

                MPI_Wait(&recvRequests[recvIdx], MPI_STATUS_IGNORE);

                if (solverCPU->getDomain()->hasDomain())
                {
                    solverCPU->setDensityDistributions(recvOrigin, recvSize, CPU, recvBuffers->at(recvIdx), &streamsCPU->at(recvIdx));
                    GPU_ERROR_CHECK(cudaStreamSynchronize(streamsCPU->at(recvIdx)))
                } else {
                    solverGPU->setDensityDistributions(recvOrigin, recvSize, CPU, recvBuffers->at(recvIdx), &streamsGPU->at(recvIdx));
                    GPU_ERROR_CHECK(cudaStreamSynchronize(streamsGPU->at(recvIdx)))
                }

                recvIdx++;
            }
            if (static_cast<size_t>(sendIdx) < communication.size() && communication[sendIdx].getDirection() == BOTTOM)
            {
                sendOrigin = communication[sendIdx].getSendOrigin();
                sendSize = communication[sendIdx].getSendSize();

                if (solverCPU->getDomain()->hasDomain())
                {
                    solverCPU->getDensityDistributions(sendOrigin, sendSize, CPU, sendBuffers->at(sendIdx), &streamsCPU->at(sendIdx));
                    GPU_ERROR_CHECK(cudaStreamSynchronize(streamsCPU->at(sendIdx)))
                } else {
                    solverGPU->getDensityDistributions(sendOrigin, sendSize, CPU, sendBuffers->at(sendIdx), &streamsGPU->at(sendIdx));
                    GPU_ERROR_CHECK(cudaStreamSynchronize(streamsGPU->at(sendIdx)))
                }

                MPI_Isend(
                        sendBuffers->at(sendIdx),
                        NUM_LATTICE_VECTORS * communication[sendIdx].getSendSize().elements(),
                        ((typeid(T) == typeid(float)) ? MPI_FLOAT : MPI_DOUBLE),
                        communication[sendIdx].getDstId(),
                        simulationStepCounter,
                        MPI_COMM_WORLD,
                        &sendRequests[sendIdx]);

                sendIdx++;
            }
            if (static_cast<size_t>(sendIdx) < communication.size() && communication[sendIdx].getDirection() == TOP)
            {
                sendOrigin = communication[sendIdx].getSendOrigin();
                sendSize = communication[sendIdx].getSendSize();

                if (solverCPU->getDomain()->hasDomain())
                {
                    solverCPU->getDensityDistributions(sendOrigin, sendSize, CPU, sendBuffers->at(sendIdx), &streamsCPU->at(sendIdx));
                    GPU_ERROR_CHECK(cudaStreamSynchronize(streamsCPU->at(sendIdx)))
                } else {
                    solverGPU->getDensityDistributions(sendOrigin, sendSize, CPU, sendBuffers->at(sendIdx), &streamsGPU->at(sendIdx));
                    GPU_ERROR_CHECK(cudaStreamSynchronize(streamsGPU->at(sendIdx)))
                }

                MPI_Isend(
                        sendBuffers->at(sendIdx),
                        NUM_LATTICE_VECTORS * communication[sendIdx].getSendSize().elements(),
                        ((typeid(T) == typeid(float)) ? MPI_FLOAT : MPI_DOUBLE),
                        communication[sendIdx].getDstId(),
                        simulationStepCounter,
                        MPI_COMM_WORLD,
                        &sendRequests[sendIdx]);

                sendIdx++;
            }
            if (static_cast<size_t>(recvIdx) < communication.size() && communication[recvIdx].getDirection() == BOTTOM)
            {
                recvOrigin = communication[recvIdx].getRecvOrigin();
                recvSize = communication[recvIdx].getRecvSize();

                MPI_Wait(&recvRequests[recvIdx], MPI_STATUS_IGNORE);

                if (solverCPU->getDomain()->hasDomain())
                {
                    solverCPU->setDensityDistributions(recvOrigin, recvSize, CPU, recvBuffers->at(recvIdx), &streamsCPU->at(recvIdx));
                    GPU_ERROR_CHECK(cudaStreamSynchronize(streamsCPU->at(recvIdx)))
                } else {
                    solverGPU->setDensityDistributions(recvOrigin, recvSize, CPU, recvBuffers->at(recvIdx), &streamsGPU->at(recvIdx));
                    GPU_ERROR_CHECK(cudaStreamSynchronize(streamsGPU->at(recvIdx)))
                }

                recvIdx++;
            }
            if (static_cast<size_t>(recvIdx) < communication.size() && communication[recvIdx].getDirection() == TOP)
            {
                recvOrigin = communication[recvIdx].getRecvOrigin();
                recvSize = communication[recvIdx].getRecvSize();

                MPI_Wait(&recvRequests[recvIdx], MPI_STATUS_IGNORE);

                if (solverCPU->getDomain()->hasDomain())
                {
                    solverCPU->setDensityDistributions(recvOrigin, recvSize, CPU, recvBuffers->at(recvIdx), &streamsCPU->at(recvIdx));
                    GPU_ERROR_CHECK(cudaStreamSynchronize(streamsCPU->at(recvIdx)))
                } else {
                    solverGPU->setDensityDistributions(recvOrigin, recvSize, CPU, recvBuffers->at(recvIdx), &streamsGPU->at(recvIdx));
                    GPU_ERROR_CHECK(cudaStreamSynchronize(streamsGPU->at(recvIdx)))
                }

                recvIdx++;
            }
            if (static_cast<size_t>(sendIdx) < communication.size() && communication[sendIdx].getDirection() == BACK)
            {
                sendOrigin = communication[sendIdx].getSendOrigin();
                sendSize = communication[sendIdx].getSendSize();

                if (solverCPU->getDomain()->hasDomain())
                {
                    solverCPU->getDensityDistributions(sendOrigin, sendSize, CPU, sendBuffers->at(sendIdx), &streamsCPU->at(sendIdx));
                    GPU_ERROR_CHECK(cudaStreamSynchronize(streamsCPU->at(sendIdx)))
                } else {
                    solverGPU->getDensityDistributions(sendOrigin, sendSize, CPU, sendBuffers->at(sendIdx), &streamsGPU->at(sendIdx));
                    GPU_ERROR_CHECK(cudaStreamSynchronize(streamsGPU->at(sendIdx)))
                }

                MPI_Isend(
                        sendBuffers->at(sendIdx),
                        NUM_LATTICE_VECTORS * communication[sendIdx].getSendSize().elements(),
                        ((typeid(T) == typeid(float)) ? MPI_FLOAT : MPI_DOUBLE),
                        communication[sendIdx].getDstId(),
                        simulationStepCounter,
                        MPI_COMM_WORLD,
                        &sendRequests[sendIdx]);

                sendIdx++;
            }
            if (static_cast<size_t>(sendIdx) < communication.size() && communication[sendIdx].getDirection() == FRONT)
            {
                sendOrigin = communication[sendIdx].getSendOrigin();
                sendSize = communication[sendIdx].getSendSize();

                if (solverCPU->getDomain()->hasDomain())
                {
                    solverCPU->getDensityDistributions(sendOrigin, sendSize, CPU, sendBuffers->at(sendIdx), &streamsCPU->at(sendIdx));
                    GPU_ERROR_CHECK(cudaStreamSynchronize(streamsCPU->at(sendIdx)))
                } else {
                    solverGPU->getDensityDistributions(sendOrigin, sendSize, CPU, sendBuffers->at(sendIdx), &streamsGPU->at(sendIdx));
                    GPU_ERROR_CHECK(cudaStreamSynchronize(streamsGPU->at(sendIdx)))
                }

                MPI_Isend(
                        sendBuffers->at(sendIdx),
                        NUM_LATTICE_VECTORS * communication[sendIdx].getSendSize().elements(),
                        ((typeid(T) == typeid(float)) ? MPI_FLOAT : MPI_DOUBLE),
                        communication[sendIdx].getDstId(),
                        simulationStepCounter,
                        MPI_COMM_WORLD,
                        &sendRequests[sendIdx]);

                sendIdx++;
            }
            if (static_cast<size_t>(recvIdx) < communication.size() && communication[recvIdx].getDirection() == BACK)
            {
                recvOrigin = communication[recvIdx].getRecvOrigin();
                recvSize = communication[recvIdx].getRecvSize();

                MPI_Wait(&recvRequests[recvIdx], MPI_STATUS_IGNORE);

                if (solverCPU->getDomain()->hasDomain())
                {
                    solverCPU->setDensityDistributions(recvOrigin, recvSize, CPU, recvBuffers->at(recvIdx), &streamsCPU->at(recvIdx));
                } else {
                    solverGPU->setDensityDistributions(recvOrigin, recvSize, CPU, recvBuffers->at(recvIdx), &streamsGPU->at(recvIdx));
                }

                recvIdx++;
            }
            if (static_cast<size_t>(recvIdx) < communication.size() && communication[recvIdx].getDirection() == FRONT)
            {
                recvOrigin = communication[recvIdx].getRecvOrigin();
                recvSize = communication[recvIdx].getRecvSize();

                MPI_Wait(&recvRequests[recvIdx], MPI_STATUS_IGNORE);

                if (solverCPU->getDomain()->hasDomain())
                {
                    solverCPU->setDensityDistributions(recvOrigin, recvSize, CPU, recvBuffers->at(recvIdx), &streamsCPU->at(recvIdx));
                } else {
                    solverGPU->setDensityDistributions(recvOrigin, recvSize, CPU, recvBuffers->at(recvIdx), &streamsGPU->at(recvIdx));
                   }

                recvIdx++;
            }
        /*
         * If there is a CPU and a GPU domain, the send buffers have to be
         * written/receive buffers have to be read in a different way. Separate
         * intra-node communication between local CPU and GPU subdomain is
         * required/necessary.
         */
#endif
        } else {
            CVector<3, int> sendOriginCPU, recvOriginCPU, sendOriginGPU, recvOriginGPU;
            CVector<3, int> sendSizeCPU, recvSizeCPU, sendSizeGPU, recvSizeGPU;
            CVector<3, int> dimCPU, dimGPU;

#ifdef USE_MPI
            if (static_cast<size_t>(sendIdx) < communication.size() && communication[sendIdx].getDirection() == LEFT)
            {
                sendOriginCPU.set(1, 0, 0);
                sendSizeCPU.set(1, solverCPU->getDomain()->getSizeWithHalo()[1] - 1, solverCPU->getDomain()->getSizeWithHalo()[2]);
                sendOriginGPU.set(1, 1, 0);
                sendSizeGPU.set(1, solverGPU->getDomain()->getSizeWithHalo()[1] - 1, solverGPU->getDomain()->getSizeWithHalo()[2]);

                solverGPU->getDensityDistributions(sendOriginGPU, sendSizeGPU, CPU, &sendBuffers->at(sendIdx)[NUM_LATTICE_VECTORS * sendSizeCPU.elements()], &streamsGPU->at(sendIdx));
                solverCPU->getDensityDistributions(sendOriginCPU, sendSizeCPU, CPU, &sendBuffers->at(sendIdx)[0], &streamsCPU->at(sendIdx));
                GPU_ERROR_CHECK(cudaStreamSynchronize(streamsGPU->at(sendIdx)))
                GPU_ERROR_CHECK(cudaStreamSynchronize(streamsCPU->at(sendIdx)))

                MPI_Isend(
                        sendBuffers->at(sendIdx),
                        NUM_LATTICE_VECTORS * communication[sendIdx].getSendSize().elements(),
                        ((typeid(T) == typeid(float)) ? MPI_FLOAT : MPI_DOUBLE),
                        communication[sendIdx].getDstId(),
                        simulationStepCounter,
                        MPI_COMM_WORLD,
                        &sendRequests[sendIdx]);

                sendIdx++;
            }
            if (static_cast<size_t>(sendIdx) < communication.size() && communication[sendIdx].getDirection() == RIGHT)
            {
                sendOriginCPU.set(solverCPU->getDomain()->getSizeWithHalo()[0] - 2, 0, 0);
                sendSizeCPU.set(1, solverCPU->getDomain()->getSizeWithHalo()[1] - 1, solverCPU->getDomain()->getSizeWithHalo()[2]);
                sendOriginGPU.set(solverGPU->getDomain()->getSizeWithHalo()[0] - 2, 1, 0);
                sendSizeGPU.set(1, solverGPU->getDomain()->getSizeWithHalo()[1] - 1, solverGPU->getDomain()->getSizeWithHalo()[2]);

                solverGPU->getDensityDistributions(sendOriginGPU, sendSizeGPU, CPU, &sendBuffers->at(sendIdx)[NUM_LATTICE_VECTORS * sendSizeCPU.elements()], &streamsGPU->at(sendIdx));
                solverCPU->getDensityDistributions(sendOriginCPU, sendSizeCPU, CPU, &sendBuffers->at(sendIdx)[0], &streamsCPU->at(sendIdx));
                GPU_ERROR_CHECK(cudaStreamSynchronize(streamsGPU->at(sendIdx)))
                GPU_ERROR_CHECK(cudaStreamSynchronize(streamsCPU->at(sendIdx)))

                MPI_Isend(
                        sendBuffers->at(sendIdx),
                        NUM_LATTICE_VECTORS * communication[sendIdx].getSendSize().elements(),
                        ((typeid(T) == typeid(float)) ? MPI_FLOAT : MPI_DOUBLE),
                        communication[sendIdx].getDstId(),
                        simulationStepCounter,
                        MPI_COMM_WORLD,
                        &sendRequests[sendIdx]);

                sendIdx++;
            }
            if (static_cast<size_t>(recvIdx) < communication.size() && communication[recvIdx].getDirection() == LEFT)
            {
                recvOriginCPU.set(0, 0, 0);
                recvSizeCPU.set(1, solverCPU->getDomain()->getSizeWithHalo()[1] - 1, solverCPU->getDomain()->getSizeWithHalo()[2]);
                recvOriginGPU.set(0, 1, 0);
                recvSizeGPU.set(1, solverGPU->getDomain()->getSizeWithHalo()[1] - 1, solverGPU->getDomain()->getSizeWithHalo()[2]);

                MPI_Wait(&recvRequests[recvIdx], MPI_STATUS_IGNORE);

                solverGPU->setDensityDistributions(recvOriginGPU, recvSizeGPU, CPU, &recvBuffers->at(recvIdx)[NUM_LATTICE_VECTORS * recvSizeCPU.elements()], &streamsGPU->at(recvIdx));
                solverCPU->setDensityDistributions(recvOriginCPU, recvSizeCPU, CPU, &recvBuffers->at(recvIdx)[0], &streamsCPU->at(recvIdx));
                GPU_ERROR_CHECK(cudaStreamSynchronize(streamsGPU->at(recvIdx)))
                GPU_ERROR_CHECK(cudaStreamSynchronize(streamsCPU->at(recvIdx)))

                recvIdx++;
            }
            if (static_cast<size_t>(recvIdx) < communication.size() && communication[recvIdx].getDirection() == RIGHT)
            {
                recvOriginCPU.set(solverCPU->getDomain()->getSizeWithHalo()[0] - 1, 0, 0);
                recvSizeCPU.set(1, solverCPU->getDomain()->getSizeWithHalo()[1] - 1, solverCPU->getDomain()->getSizeWithHalo()[2]);
                recvOriginGPU.set(solverGPU->getDomain()->getSizeWithHalo()[0] - 1, 1, 0);
                recvSizeGPU.set(1, solverGPU->getDomain()->getSizeWithHalo()[1] - 1, solverGPU->getDomain()->getSizeWithHalo()[2]);

                MPI_Wait(&recvRequests[recvIdx], MPI_STATUS_IGNORE);

                solverGPU->setDensityDistributions(recvOriginGPU, recvSizeGPU, CPU, &recvBuffers->at(recvIdx)[NUM_LATTICE_VECTORS * recvSizeCPU.elements()], &streamsGPU->at(recvIdx));
                solverCPU->setDensityDistributions(recvOriginCPU, recvSizeCPU, CPU, &recvBuffers->at(recvIdx)[0], &streamsCPU->at(recvIdx));
                GPU_ERROR_CHECK(cudaStreamSynchronize(streamsGPU->at(recvIdx)))
                GPU_ERROR_CHECK(cudaStreamSynchronize(streamsCPU->at(recvIdx)))

                recvIdx++;
            }
            if (static_cast<size_t>(sendIdx) < communication.size() && communication[sendIdx].getDirection() == BOTTOM)
            {
                sendOriginCPU.set(0, 1, 0);
                sendSizeCPU.set(solverCPU->getDomain()->getSizeWithHalo()[0], 1, solverCPU->getDomain()->getSizeWithHalo()[2]);

                solverCPU->getDensityDistributions(sendOriginCPU, sendSizeCPU, CPU, sendBuffers->at(sendIdx), &streamsCPU->at(sendIdx));
                GPU_ERROR_CHECK(cudaStreamSynchronize(streamsCPU->at(sendIdx)))

                MPI_Isend(
                        sendBuffers->at(sendIdx),
                        NUM_LATTICE_VECTORS * communication[sendIdx].getSendSize().elements(),
                        ((typeid(T) == typeid(float)) ? MPI_FLOAT : MPI_DOUBLE),
                        communication[sendIdx].getDstId(),
                        simulationStepCounter,
                        MPI_COMM_WORLD,
                        &sendRequests[sendIdx]);

                sendIdx++;
            }
            if (static_cast<size_t>(sendIdx) < communication.size() && communication[sendIdx].getDirection() == TOP)
            {
                sendOriginGPU.set(0, solverGPU->getDomain()->getSizeWithHalo()[1] - 2, 0);
                sendSizeGPU.set(solverGPU->getDomain()->getSizeWithHalo()[0], 1, solverGPU->getDomain()->getSizeWithHalo()[2]);

                solverGPU->getDensityDistributions(sendOriginGPU, sendSizeGPU, CPU, sendBuffers->at(sendIdx), &streamsGPU->at(sendIdx));
                GPU_ERROR_CHECK(cudaStreamSynchronize(streamsGPU->at(sendIdx)))

                MPI_Isend(
                        sendBuffers->at(sendIdx),
                        NUM_LATTICE_VECTORS * communication[sendIdx].getSendSize().elements(),
                        ((typeid(T) == typeid(float)) ? MPI_FLOAT : MPI_DOUBLE),
                        communication[sendIdx].getDstId(),
                        simulationStepCounter,
                        MPI_COMM_WORLD,
                        &sendRequests[sendIdx]);

                sendIdx++;
            }
            if (static_cast<size_t>(recvIdx) < communication.size() && communication[recvIdx].getDirection() == BOTTOM)
            {
                recvOriginCPU.set(0, 0, 0);
                recvSizeCPU.set(solverCPU->getDomain()->getSizeWithHalo()[0], 1, solverCPU->getDomain()->getSizeWithHalo()[2]);

                MPI_Wait(&recvRequests[recvIdx], MPI_STATUS_IGNORE);

                solverCPU->setDensityDistributions(recvOriginCPU, recvSizeCPU, CPU, recvBuffers->at(recvIdx), &streamsCPU->at(recvIdx));
                GPU_ERROR_CHECK(cudaStreamSynchronize(streamsCPU->at(recvIdx)))

                recvIdx++;
            }
            if (static_cast<size_t>(recvIdx) < communication.size() && communication[recvIdx].getDirection() == TOP)
            {
                recvOriginGPU.set(0, solverGPU->getDomain()->getSizeWithHalo()[1] - 1, 0);
                recvSizeGPU.set(solverGPU->getDomain()->getSizeWithHalo()[0], 1, solverGPU->getDomain()->getSizeWithHalo()[2]);

                MPI_Wait(&recvRequests[recvIdx], MPI_STATUS_IGNORE);

                solverGPU->setDensityDistributions(recvOriginGPU, recvSizeGPU, CPU, recvBuffers->at(recvIdx), &streamsGPU->at(recvIdx));
                GPU_ERROR_CHECK(cudaStreamSynchronize(streamsGPU->at(recvIdx)))

                recvIdx++;
            }
            if (static_cast<size_t>(sendIdx) < communication.size() && communication[sendIdx].getDirection() == BACK)
            {
                sendOriginCPU.set(0, 0, 1);
                sendSizeCPU.set(solverCPU->getDomain()->getSizeWithHalo()[0], solverCPU->getDomain()->getSizeWithHalo()[1] - 1, 1);
                sendOriginGPU.set(0, 1, 1);
                sendSizeGPU.set(solverGPU->getDomain()->getSizeWithHalo()[0], solverGPU->getDomain()->getSizeWithHalo()[1] - 1, 1);

                solverGPU->getDensityDistributions(sendOriginGPU, sendSizeGPU, CPU, &sendBuffers->at(sendIdx)[NUM_LATTICE_VECTORS * sendSizeCPU.elements()], &streamsGPU->at(sendIdx));
                solverCPU->getDensityDistributions(sendOriginCPU, sendSizeCPU, CPU, &sendBuffers->at(sendIdx)[0], &streamsCPU->at(sendIdx));
                GPU_ERROR_CHECK(cudaStreamSynchronize(streamsGPU->at(sendIdx)))
                GPU_ERROR_CHECK(cudaStreamSynchronize(streamsCPU->at(sendIdx)))

                MPI_Isend(
                        sendBuffers->at(sendIdx),
                        NUM_LATTICE_VECTORS * communication[sendIdx].getSendSize().elements(),
                        ((typeid(T) == typeid(float)) ? MPI_FLOAT : MPI_DOUBLE),
                        communication[sendIdx].getDstId(),
                        simulationStepCounter,
                        MPI_COMM_WORLD,
                        &sendRequests[sendIdx]);

                sendIdx++;
            }
            if (static_cast<size_t>(sendIdx) < communication.size() && communication[sendIdx].getDirection() == FRONT)
            {
                sendOriginCPU.set(0, 0, solverCPU->getDomain()->getSizeWithHalo()[2] - 2);
                sendSizeCPU.set(solverCPU->getDomain()->getSizeWithHalo()[0], solverCPU->getDomain()->getSizeWithHalo()[1] - 1, 1);
                sendOriginGPU.set(0, 1, solverGPU->getDomain()->getSizeWithHalo()[2] - 2);
                sendSizeGPU.set(solverGPU->getDomain()->getSizeWithHalo()[0], solverGPU->getDomain()->getSizeWithHalo()[1] - 1, 1);

                solverGPU->getDensityDistributions(sendOriginGPU, sendSizeGPU, CPU, &sendBuffers->at(sendIdx)[NUM_LATTICE_VECTORS * sendSizeCPU.elements()], &streamsGPU->at(sendIdx));
                solverCPU->getDensityDistributions(sendOriginCPU, sendSizeCPU, CPU, &sendBuffers->at(sendIdx)[0], &streamsCPU->at(sendIdx));
                GPU_ERROR_CHECK(cudaStreamSynchronize(streamsGPU->at(sendIdx)))
                GPU_ERROR_CHECK(cudaStreamSynchronize(streamsCPU->at(sendIdx)))

                MPI_Isend(
                        sendBuffers->at(sendIdx),
                        NUM_LATTICE_VECTORS * communication[sendIdx].getSendSize().elements(),
                        ((typeid(T) == typeid(float)) ? MPI_FLOAT : MPI_DOUBLE),
                        communication[sendIdx].getDstId(),
                        simulationStepCounter,
                        MPI_COMM_WORLD,
                        &sendRequests[sendIdx]);

                sendIdx++;
            }
            if (static_cast<size_t>(recvIdx) < communication.size() && communication[recvIdx].getDirection() == BACK)
            {
                recvOriginCPU.set(0, 0, 0);
                recvSizeCPU.set(solverCPU->getDomain()->getSizeWithHalo()[0], solverCPU->getDomain()->getSizeWithHalo()[1] - 1, 1);
                recvOriginGPU.set(0, 1, 0);
                recvSizeGPU.set(solverGPU->getDomain()->getSizeWithHalo()[0], solverGPU->getDomain()->getSizeWithHalo()[1] - 1, 1);

                MPI_Wait(&recvRequests[recvIdx], MPI_STATUS_IGNORE);

                solverGPU->setDensityDistributions(recvOriginGPU, recvSizeGPU, CPU, &recvBuffers->at(recvIdx)[NUM_LATTICE_VECTORS * recvSizeCPU.elements()], &streamsGPU->at(recvIdx));
                solverCPU->setDensityDistributions(recvOriginCPU, recvSizeCPU, CPU, &recvBuffers->at(recvIdx)[0], &streamsCPU->at(recvIdx));

                recvIdx++;
            }
            if (static_cast<size_t>(recvIdx) < communication.size() && communication[recvIdx].getDirection() == FRONT)
            {
                recvOriginCPU.set(0, 0, solverCPU->getDomain()->getSizeWithHalo()[2] - 1);
                recvSizeCPU.set(solverCPU->getDomain()->getSizeWithHalo()[0], solverCPU->getDomain()->getSizeWithHalo()[1] - 1, 1);
                recvOriginGPU.set(0, 1, solverGPU->getDomain()->getSizeWithHalo()[2] - 1);
                recvSizeGPU.set(solverGPU->getDomain()->getSizeWithHalo()[0], solverGPU->getDomain()->getSizeWithHalo()[1] - 1, 1);

                MPI_Wait(&recvRequests[recvIdx], MPI_STATUS_IGNORE);

                solverGPU->setDensityDistributions(recvOriginGPU, recvSizeGPU, CPU, &recvBuffers->at(recvIdx)[NUM_LATTICE_VECTORS * recvSizeCPU.elements()], &streamsGPU->at(recvIdx));
                solverCPU->setDensityDistributions(recvOriginCPU, recvSizeCPU, CPU, &recvBuffers->at(recvIdx)[0], &streamsCPU->at(recvIdx));

                recvIdx++;
            }
#endif
            sendOriginCPU.set(0, solverCPU->getDomain()->getSizeWithHalo()[1] - 2, 0);
            recvOriginCPU.set(0, solverCPU->getDomain()->getSizeWithHalo()[1] - 1, 0);
            sendOriginGPU.set(0, 1, 0);
            recvOriginGPU.set(0, 0, 0);
            sendSizeCPU.set(solverCPU->getDomain()->getSizeWithHalo()[0], 1, solverCPU->getDomain()->getSizeWithHalo()[2]);
            recvSizeCPU.set(solverCPU->getDomain()->getSizeWithHalo()[0], 1, solverCPU->getDomain()->getSizeWithHalo()[2]);
            sendSizeGPU.set(solverGPU->getDomain()->getSizeWithHalo()[0], 1, solverGPU->getDomain()->getSizeWithHalo()[2]);
            recvSizeGPU.set(solverGPU->getDomain()->getSizeWithHalo()[0], 1, solverGPU->getDomain()->getSizeWithHalo()[2]);
            dimCPU = solverCPU->getDomain()->getSizeWithHalo();
            dimGPU = solverGPU->getDomain()->getSizeWithHalo();

            solverGPU->setDensityDistributions(recvSizeGPU, sendOriginCPU, dimCPU, CPU, recvOriginGPU, solverCPU->getDensityDistributions(), &CPUtoGPUStream);
            solverCPU->setDensityDistributions(recvSizeCPU, sendOriginGPU, dimGPU, GPU, recvOriginCPU, solverGPU->getDensityDistributions(), &GPUtoCPUStream);
        }
#ifdef USE_MPI

        /*
         * Explicit synchronization point after all MPI send buffers can be safely
         * re-used again which means: Communication with neighbors has finished.
         */
        MPI_Waitall(communication.size(), sendRequests, MPI_STATUS_IGNORE);
#endif
    }
    /*
     * Implicit synchronization point after OpenMP parallel region after inner
     * region of the CPU has been processed.
     */

    /*
     * Explicit synchronization point after inner region of the GPU has been
     * processed.
     */
    GPU_ERROR_CHECK(cudaStreamSynchronize(innerStream))
    /*
     * Explicit synchronization point to complete communication between CPU and
     * GPU part of a subdomain.
     */
    if (solverCPU->getDomain()->hasDomain() && solverGPU->getDomain()->hasDomain())
    {
        GPU_ERROR_CHECK(cudaStreamSynchronize(GPUtoCPUStream))
        GPU_ERROR_CHECK(cudaStreamSynchronize(CPUtoGPUStream))
    }

    if (configuration->doLogging)
    {
        std::stringstream loggingFileName;
        loggingFileName << configuration->loggingOutputDir << "/log_" << id << ".txt";
        std::ofstream loggingFile(loggingFileName.str().c_str(), std::ios::out | std::ios::app);
        if (loggingFile.is_open())
        {
            loggingFile << "Alpha step successful." << std::endl;
            loggingFile << "---------------------------------------" << std::endl;
            loggingFile.close();
        } else {
            std::cerr << "----- CController<T>::stepAlpha() -----" << std::endl;
            std::cerr << "There is no open file to write logs." << std::endl;
            std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
            std::cerr << "---------------------------------------" << std::endl;

            exit (EXIT_FAILURE);
        }
    }
}

template <class T>
void CController<T>::stepBeta()
{
    if (configuration->doLogging)
    {
        std::stringstream loggingFileName;
        loggingFileName << configuration->loggingOutputDir << "/log_" << id << ".txt";
        std::ofstream loggingFile(loggingFileName.str().c_str(), std::ios::out | std::ios::app);
        if (loggingFile.is_open())
        {
            loggingFile << "----- CController<T>::stepBeta() -----" << std::endl;
            loggingFile.close();
        } else {
            std::cerr << "----- CController<T>::stepBeta() -----" << std::endl;
            std::cerr << "There is no open file to write logs." << std::endl;
            std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
            std::cerr << "--------------------------------------" << std::endl;

            exit (EXIT_FAILURE);
        }
    }

#ifdef USE_MPI
    for (unsigned int i = 0; i < communication.size(); i++)
    {
        MPI_Irecv(
                recvBuffers->at(i),
                NUM_LATTICE_VECTORS * communication[i].getRecvSize().elements(),
                ((typeid(T) == typeid(float)) ? MPI_FLOAT : MPI_DOUBLE),
                communication[i].getDstId(),
                simulationStepCounter,
                MPI_COMM_WORLD,
                &recvRequests[i]);
    }
#endif

    /*
     * Compute all boundary regions of the GPU domain.
     */
    for (unsigned int i = 0; i < boundaryOriginsGPU->size(); i++)
    {
        solverGPU->simulationStepBeta(boundaryOriginsGPU->at(i), boundarySizesGPU->at(i), &streamsGPU->at(i));

        if (configuration->doLogging)
        {
            std::stringstream loggingFileName;
            loggingFileName << configuration->loggingOutputDir << "/log_" << id << ".txt";
            std::ofstream loggingFile(loggingFileName.str().c_str(), std::ios::out | std::ios::app);
            if (loggingFile.is_open())
            {
                loggingFile << "The following GPU boundary was issued to be computed:" << std::endl;
                loggingFile << "boundary origin:        " << boundaryOriginsGPU->at(i) << std::endl;
                loggingFile << "boundary size:          " << boundarySizesGPU->at(i) << std::endl;
                loggingFile << "--------------------------------------" << std::endl;
                loggingFile.close();
            } else {
                std::cerr << "----- CController<T>::stepBeta() -----" << std::endl;
                std::cerr << "There is no open file to write logs." << std::endl;
                std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
                std::cerr << "--------------------------------------" << std::endl;

                exit (EXIT_FAILURE);
            }
        }
    }

    /*
     * Compute all boundary regions of the CPU domain.
     */
#pragma omp parallel
#pragma omp single
    {
        for (unsigned int i = 0; i < boundaryOriginsCPU->size(); i++)
        {
            solverCPU->simulationStepBeta(boundaryOriginsCPU->at(i), boundarySizesCPU->at(i));

            if (configuration->doLogging)
            {
                std::stringstream loggingFileName;
                loggingFileName << configuration->loggingOutputDir << "/log_" << id << ".txt";
                std::ofstream loggingFile(loggingFileName.str().c_str(), std::ios::out | std::ios::app);
                if (loggingFile.is_open())
                {
                    loggingFile << "The following CPU boundary was issued to be computed:" << std::endl;
                    loggingFile << "boundary origin:        " << boundaryOriginsCPU->at(i) << std::endl;
                    loggingFile << "boundary size:          " << boundarySizesCPU->at(i) << std::endl;
                    loggingFile << "--------------------------------------" << std::endl;
                    loggingFile.close();
                } else {
                    std::cerr << "----- CController<T>::stepBeta() -----" << std::endl;
                    std::cerr << "There is no open file to write logs." << std::endl;
                    std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
                    std::cerr << "--------------------------------------" << std::endl;

                    exit (EXIT_FAILURE);
                }
            }
        }
    }
    /*
     * Implicit synchronization point after OpenMP parallel region after all
     * boundary regions of the CPU have been processed.
     */

    /*
     * Explicit synchronization point after all boundary regions of the GPU
     * have been processed.
     */
    GPU_ERROR_CHECK(cudaDeviceSynchronize())

    /*
     * Compute inner region of GPU domain. This call has to be done outside the
     * following parallel region because innerStream is also created outside a
     * parallel region. This launch is asynchronous since a stream is passed.
     */
    solverGPU->simulationStepBeta(innerOriginGPU, innerSizeGPU, &innerStream);

#pragma omp parallel
#pragma omp single
    {
        /*
         * Compute inner region of CPU domain. This launch is asynchronous since
         * a single master task spawns new tasks.
         */
        solverCPU->simulationStepBeta(innerOriginCPU, innerSizeCPU);

#ifdef USE_MPI
        int sendIdx, recvIdx;

        sendIdx = recvIdx = 0;

        /*
         * If there is only a CPU or a GPU domain, the send/receive buffers can
         * be handled in a straight forward way.
         */
#endif
        if (solverCPU->getDomain()->hasDomain() ^ solverGPU->getDomain()->hasDomain())
        {
#ifdef USE_MPI
            CVector<3, int> sendOrigin, recvOrigin;
            CVector<3, int> sendSize, recvSize;

            if (static_cast<size_t>(sendIdx) < communication.size() && communication[sendIdx].getDirection() == LEFT)
            {
                sendOrigin = communication[sendIdx].getRecvOrigin();
                sendSize = communication[sendIdx].getRecvSize();

                if (solverCPU->getDomain()->hasDomain())
                {
                    solverCPU->getDensityDistributions(sendOrigin, sendSize, CPU, sendBuffers->at(sendIdx), &streamsCPU->at(sendIdx));
                    GPU_ERROR_CHECK(cudaStreamSynchronize(streamsCPU->at(sendIdx)))
                } else {
                    solverGPU->getDensityDistributions(sendOrigin, sendSize, CPU, sendBuffers->at(sendIdx), &streamsGPU->at(sendIdx));
                    GPU_ERROR_CHECK(cudaStreamSynchronize(streamsGPU->at(sendIdx)))
                }

                MPI_Isend(
                        sendBuffers->at(sendIdx),
                        NUM_LATTICE_VECTORS * communication[sendIdx].getSendSize().elements(),
                        ((typeid(T) == typeid(float)) ? MPI_FLOAT : MPI_DOUBLE),
                        communication[sendIdx].getDstId(),
                        simulationStepCounter,
                        MPI_COMM_WORLD,
                        &sendRequests[sendIdx]);

                sendIdx++;
            }
            if (static_cast<size_t>(sendIdx) < communication.size() && communication[sendIdx].getDirection() == RIGHT)
            {
                sendOrigin = communication[sendIdx].getRecvOrigin();
                sendSize = communication[sendIdx].getRecvSize();

                if (solverCPU->getDomain()->hasDomain())
                {
                    solverCPU->getDensityDistributions(sendOrigin, sendSize, CPU, sendBuffers->at(sendIdx), &streamsCPU->at(sendIdx));
                    GPU_ERROR_CHECK(cudaStreamSynchronize(streamsCPU->at(sendIdx)))
                } else {
                    solverGPU->getDensityDistributions(sendOrigin, sendSize, CPU, sendBuffers->at(sendIdx), &streamsGPU->at(sendIdx));
                    GPU_ERROR_CHECK(cudaStreamSynchronize(streamsGPU->at(sendIdx)))
                }

                MPI_Isend(
                        sendBuffers->at(sendIdx),
                        NUM_LATTICE_VECTORS * communication[sendIdx].getSendSize().elements(),
                        ((typeid(T) == typeid(float)) ? MPI_FLOAT : MPI_DOUBLE),
                        communication[sendIdx].getDstId(),
                        simulationStepCounter,
                        MPI_COMM_WORLD,
                        &sendRequests[sendIdx]);

                sendIdx++;
            }
            if (static_cast<size_t>(recvIdx) < communication.size() && communication[recvIdx].getDirection() == LEFT)
            {
                recvOrigin = communication[recvIdx].getSendOrigin();
                recvSize = communication[recvIdx].getSendSize();

                MPI_Wait(&recvRequests[recvIdx], MPI_STATUS_IGNORE);

                if (solverCPU->getDomain()->hasDomain())
                {
                    solverCPU->setDensityDistributions(recvOrigin, recvSize, CPU, communication[recvIdx].getDirection(), recvBuffers->at(recvIdx), &streamsCPU->at(recvIdx));
                    GPU_ERROR_CHECK(cudaStreamSynchronize(streamsCPU->at(recvIdx)))
                } else {
                    solverGPU->setDensityDistributions(recvOrigin, recvSize, CPU, communication[recvIdx].getDirection(), recvBuffers->at(recvIdx), &streamsGPU->at(recvIdx));
                    GPU_ERROR_CHECK(cudaStreamSynchronize(streamsGPU->at(recvIdx)))
                }

                recvIdx++;
            }
            if (static_cast<size_t>(recvIdx) < communication.size() && communication[recvIdx].getDirection() == RIGHT)
            {
                recvOrigin = communication[recvIdx].getSendOrigin();
                recvSize = communication[recvIdx].getSendSize();

                MPI_Wait(&recvRequests[recvIdx], MPI_STATUS_IGNORE);

                if (solverCPU->getDomain()->hasDomain())
                {
                    solverCPU->setDensityDistributions(recvOrigin, recvSize, CPU, communication[recvIdx].getDirection(), recvBuffers->at(recvIdx), &streamsCPU->at(recvIdx));
                    GPU_ERROR_CHECK(cudaStreamSynchronize(streamsCPU->at(recvIdx)))
                } else {
                    solverGPU->setDensityDistributions(recvOrigin, recvSize, CPU, communication[recvIdx].getDirection(), recvBuffers->at(recvIdx), &streamsGPU->at(recvIdx));
                    GPU_ERROR_CHECK(cudaStreamSynchronize(streamsGPU->at(recvIdx)))
                }

                recvIdx++;
            }
            if (static_cast<size_t>(sendIdx) < communication.size() && communication[sendIdx].getDirection() == BOTTOM)
            {
                sendOrigin = communication[sendIdx].getRecvOrigin();
                sendSize = communication[sendIdx].getRecvSize();

                if (solverCPU->getDomain()->hasDomain())
                {
                    solverCPU->getDensityDistributions(sendOrigin, sendSize, CPU, sendBuffers->at(sendIdx), &streamsCPU->at(sendIdx));
                    GPU_ERROR_CHECK(cudaStreamSynchronize(streamsCPU->at(sendIdx)))
                } else {
                    solverGPU->getDensityDistributions(sendOrigin, sendSize, CPU, sendBuffers->at(sendIdx), &streamsGPU->at(sendIdx));
                    GPU_ERROR_CHECK(cudaStreamSynchronize(streamsGPU->at(sendIdx)))
                }

                MPI_Isend(
                        sendBuffers->at(sendIdx),
                        NUM_LATTICE_VECTORS * communication[sendIdx].getSendSize().elements(),
                        ((typeid(T) == typeid(float)) ? MPI_FLOAT : MPI_DOUBLE),
                        communication[sendIdx].getDstId(),
                        simulationStepCounter,
                        MPI_COMM_WORLD,
                        &sendRequests[sendIdx]);

                sendIdx++;
            }
            if (static_cast<size_t>(sendIdx) < communication.size() && communication[sendIdx].getDirection() == TOP)
            {
                sendOrigin = communication[sendIdx].getRecvOrigin();
                sendSize = communication[sendIdx].getRecvSize();

                if (solverCPU->getDomain()->hasDomain())
                {
                    solverCPU->getDensityDistributions(sendOrigin, sendSize, CPU, sendBuffers->at(sendIdx), &streamsCPU->at(sendIdx));
                    GPU_ERROR_CHECK(cudaStreamSynchronize(streamsCPU->at(sendIdx)))
                } else {
                    solverGPU->getDensityDistributions(sendOrigin, sendSize, CPU, sendBuffers->at(sendIdx), &streamsGPU->at(sendIdx));
                    GPU_ERROR_CHECK(cudaStreamSynchronize(streamsGPU->at(sendIdx)))
                }

                MPI_Isend(
                        sendBuffers->at(sendIdx),
                        NUM_LATTICE_VECTORS * communication[sendIdx].getSendSize().elements(),
                        ((typeid(T) == typeid(float)) ? MPI_FLOAT : MPI_DOUBLE),
                        communication[sendIdx].getDstId(),
                        simulationStepCounter,
                        MPI_COMM_WORLD,
                        &sendRequests[sendIdx]);

                sendIdx++;
            }
            if (static_cast<size_t>(recvIdx) < communication.size() && communication[recvIdx].getDirection() == BOTTOM)
            {
                recvOrigin = communication[recvIdx].getSendOrigin();
                recvSize = communication[recvIdx].getSendSize();

                MPI_Wait(&recvRequests[recvIdx], MPI_STATUS_IGNORE);

                if (solverCPU->getDomain()->hasDomain())
                {
                    solverCPU->setDensityDistributions(recvOrigin, recvSize, CPU, communication[recvIdx].getDirection(), recvBuffers->at(recvIdx), &streamsCPU->at(recvIdx));
                    GPU_ERROR_CHECK(cudaStreamSynchronize(streamsCPU->at(recvIdx)))
                } else {
                    solverGPU->setDensityDistributions(recvOrigin, recvSize, CPU, communication[recvIdx].getDirection(), recvBuffers->at(recvIdx), &streamsGPU->at(recvIdx));
                    GPU_ERROR_CHECK(cudaStreamSynchronize(streamsGPU->at(recvIdx)))
                }

                recvIdx++;
            }
            if (static_cast<size_t>(recvIdx) < communication.size() && communication[recvIdx].getDirection() == TOP)
            {
                recvOrigin = communication[recvIdx].getSendOrigin();
                recvSize = communication[recvIdx].getSendSize();

                MPI_Wait(&recvRequests[recvIdx], MPI_STATUS_IGNORE);

                if (solverCPU->getDomain()->hasDomain())
                {
                    solverCPU->setDensityDistributions(recvOrigin, recvSize, CPU, communication[recvIdx].getDirection(), recvBuffers->at(recvIdx), &streamsCPU->at(recvIdx));
                    GPU_ERROR_CHECK(cudaStreamSynchronize(streamsCPU->at(recvIdx)))
                } else {
                    solverGPU->setDensityDistributions(recvOrigin, recvSize, CPU, communication[recvIdx].getDirection(), recvBuffers->at(recvIdx), &streamsGPU->at(recvIdx));
                    GPU_ERROR_CHECK(cudaStreamSynchronize(streamsGPU->at(recvIdx)))
                }

                recvIdx++;
            }
            if (static_cast<size_t>(sendIdx) < communication.size() && communication[sendIdx].getDirection() == BACK)
            {
                sendOrigin = communication[sendIdx].getRecvOrigin();
                sendSize = communication[sendIdx].getRecvSize();

                if (solverCPU->getDomain()->hasDomain())
                {
                    solverCPU->getDensityDistributions(sendOrigin, sendSize, CPU, sendBuffers->at(sendIdx), &streamsCPU->at(sendIdx));
                    GPU_ERROR_CHECK(cudaStreamSynchronize(streamsCPU->at(sendIdx)))
                } else {
                    solverGPU->getDensityDistributions(sendOrigin, sendSize, CPU, sendBuffers->at(sendIdx), &streamsGPU->at(sendIdx));
                    GPU_ERROR_CHECK(cudaStreamSynchronize(streamsGPU->at(sendIdx)))
                }

                MPI_Isend(
                        sendBuffers->at(sendIdx),
                        NUM_LATTICE_VECTORS * communication[sendIdx].getSendSize().elements(),
                        ((typeid(T) == typeid(float)) ? MPI_FLOAT : MPI_DOUBLE),
                        communication[sendIdx].getDstId(),
                        simulationStepCounter,
                        MPI_COMM_WORLD,
                        &sendRequests[sendIdx]);

                sendIdx++;
            }
            if (static_cast<size_t>(sendIdx) < communication.size() && communication[sendIdx].getDirection() == FRONT)
            {
                sendOrigin = communication[sendIdx].getRecvOrigin();
                sendSize = communication[sendIdx].getRecvSize();

                if (solverCPU->getDomain()->hasDomain())
                {
                    solverCPU->getDensityDistributions(sendOrigin, sendSize, CPU, sendBuffers->at(sendIdx), &streamsCPU->at(sendIdx));
                    GPU_ERROR_CHECK(cudaStreamSynchronize(streamsCPU->at(sendIdx)))
                } else {
                    solverGPU->getDensityDistributions(sendOrigin, sendSize, CPU, sendBuffers->at(sendIdx), &streamsGPU->at(sendIdx));
                    GPU_ERROR_CHECK(cudaStreamSynchronize(streamsGPU->at(sendIdx)))
                }

                MPI_Isend(
                        sendBuffers->at(sendIdx),
                        NUM_LATTICE_VECTORS * communication[sendIdx].getSendSize().elements(),
                        ((typeid(T) == typeid(float)) ? MPI_FLOAT : MPI_DOUBLE),
                        communication[sendIdx].getDstId(),
                        simulationStepCounter,
                        MPI_COMM_WORLD,
                        &sendRequests[sendIdx]);

                sendIdx++;
            }
            if (static_cast<size_t>(recvIdx) < communication.size() && communication[recvIdx].getDirection() == BACK)
            {
                recvOrigin = communication[recvIdx].getSendOrigin();
                recvSize = communication[recvIdx].getSendSize();

                MPI_Wait(&recvRequests[recvIdx], MPI_STATUS_IGNORE);

                if (solverCPU->getDomain()->hasDomain())
                {
                    solverCPU->setDensityDistributions(recvOrigin, recvSize, CPU, communication[recvIdx].getDirection(), recvBuffers->at(recvIdx), &streamsCPU->at(recvIdx));
                } else {
                    solverGPU->setDensityDistributions(recvOrigin, recvSize, CPU, communication[recvIdx].getDirection(), recvBuffers->at(recvIdx), &streamsGPU->at(recvIdx));
                }

                recvIdx++;
            }
            if (static_cast<size_t>(recvIdx) < communication.size() && communication[recvIdx].getDirection() == FRONT)
            {
                recvOrigin = communication[recvIdx].getSendOrigin();
                recvSize = communication[recvIdx].getSendSize();

                MPI_Wait(&recvRequests[recvIdx], MPI_STATUS_IGNORE);

                if (solverCPU->getDomain()->hasDomain())
                {
                    solverCPU->setDensityDistributions(recvOrigin, recvSize, CPU, communication[recvIdx].getDirection(), recvBuffers->at(recvIdx), &streamsCPU->at(recvIdx));
                } else {
                    solverGPU->setDensityDistributions(recvOrigin, recvSize, CPU, communication[recvIdx].getDirection(), recvBuffers->at(recvIdx), &streamsGPU->at(recvIdx));
                }

                recvIdx++;
            }
        /*
         * If there is a CPU and a GPU domain, the send buffers have to be
         * written/receive buffers have to be read in a different way. Separate
         * intra-node communication between local CPU and GPU subdomain is
         * required/necessary.
         */
#endif
        } else {
            CVector<3, int> sendOriginCPU, recvOriginCPU, sendOriginGPU, recvOriginGPU;
            CVector<3, int> sendSizeCPU, recvSizeCPU, sendSizeGPU, recvSizeGPU;
            CVector<3, int> dimCPU, dimGPU;

#ifdef USE_MPI
            if (static_cast<size_t>(sendIdx) < communication.size() && communication[sendIdx].getDirection() == LEFT)
            {
                sendOriginCPU.set(0, 0, 0);
                sendSizeCPU.set(1, solverCPU->getDomain()->getSizeWithHalo()[1], solverCPU->getDomain()->getSizeWithHalo()[2]);
                sendOriginGPU.set(0, 0, 0);
                sendSizeGPU.set(1, solverGPU->getDomain()->getSizeWithHalo()[1], solverGPU->getDomain()->getSizeWithHalo()[2]);

                solverGPU->getDensityDistributions(sendOriginGPU, sendSizeGPU, CPU, &sendBuffers->at(sendIdx)[NUM_LATTICE_VECTORS * sendSizeCPU.elements()], &streamsGPU->at(sendIdx));
                solverCPU->getDensityDistributions(sendOriginCPU, sendSizeCPU, CPU, &sendBuffers->at(sendIdx)[0], &streamsCPU->at(sendIdx));
                GPU_ERROR_CHECK(cudaStreamSynchronize(streamsGPU->at(sendIdx)))
                GPU_ERROR_CHECK(cudaStreamSynchronize(streamsCPU->at(sendIdx)))

                MPI_Isend(
                        sendBuffers->at(sendIdx),
                        NUM_LATTICE_VECTORS * communication[sendIdx].getSendSize().elements(),
                        ((typeid(T) == typeid(float)) ? MPI_FLOAT : MPI_DOUBLE),
                        communication[sendIdx].getDstId(),
                        simulationStepCounter,
                        MPI_COMM_WORLD,
                        &sendRequests[sendIdx]);

                sendIdx++;
            }
            if (static_cast<size_t>(sendIdx) < communication.size() && communication[sendIdx].getDirection() == RIGHT)
            {
                sendOriginCPU.set(solverCPU->getDomain()->getSizeWithHalo()[0] - 1, 0, 0);
                sendSizeCPU.set(1, solverCPU->getDomain()->getSizeWithHalo()[1], solverCPU->getDomain()->getSizeWithHalo()[2]);
                sendOriginGPU.set(solverGPU->getDomain()->getSizeWithHalo()[0] - 1, 0, 0);
                sendSizeGPU.set(1, solverGPU->getDomain()->getSizeWithHalo()[1], solverGPU->getDomain()->getSizeWithHalo()[2]);

                solverGPU->getDensityDistributions(sendOriginGPU, sendSizeGPU, CPU, &sendBuffers->at(sendIdx)[NUM_LATTICE_VECTORS * sendSizeCPU.elements()], &streamsGPU->at(sendIdx));
                solverCPU->getDensityDistributions(sendOriginCPU, sendSizeCPU, CPU, &sendBuffers->at(sendIdx)[0], &streamsCPU->at(sendIdx));
                GPU_ERROR_CHECK(cudaStreamSynchronize(streamsGPU->at(sendIdx)))
                GPU_ERROR_CHECK(cudaStreamSynchronize(streamsCPU->at(sendIdx)))

                MPI_Isend(
                        sendBuffers->at(sendIdx),
                        NUM_LATTICE_VECTORS * communication[sendIdx].getSendSize().elements(),
                        ((typeid(T) == typeid(float)) ? MPI_FLOAT : MPI_DOUBLE),
                        communication[sendIdx].getDstId(),
                        simulationStepCounter,
                        MPI_COMM_WORLD,
                        &sendRequests[sendIdx]);

                sendIdx++;
            }
            if (static_cast<size_t>(recvIdx) < communication.size() && communication[recvIdx].getDirection() == LEFT)
            {
                recvOriginCPU.set(1, 0, 0);
                recvSizeCPU.set(1, solverCPU->getDomain()->getSizeWithHalo()[1], solverCPU->getDomain()->getSizeWithHalo()[2]);
                recvOriginGPU.set(1, 0, 0);
                recvSizeGPU.set(1, solverGPU->getDomain()->getSizeWithHalo()[1], solverGPU->getDomain()->getSizeWithHalo()[2]);

                MPI_Wait(&recvRequests[recvIdx], MPI_STATUS_IGNORE);

                solverGPU->setDensityDistributions(recvOriginGPU, recvSizeGPU, CPU, communication[recvIdx].getDirection(), &recvBuffers->at(recvIdx)[NUM_LATTICE_VECTORS * recvSizeCPU.elements()], &streamsGPU->at(recvIdx));
                solverCPU->setDensityDistributions(recvOriginCPU, recvSizeCPU, CPU, communication[recvIdx].getDirection(), &recvBuffers->at(recvIdx)[0], &streamsCPU->at(recvIdx));
                GPU_ERROR_CHECK(cudaStreamSynchronize(streamsGPU->at(recvIdx)))
                GPU_ERROR_CHECK(cudaStreamSynchronize(streamsCPU->at(recvIdx)))

                recvIdx++;
            }
            if (static_cast<size_t>(recvIdx) < communication.size() && communication[recvIdx].getDirection() == RIGHT)
            {
                recvOriginCPU.set(solverCPU->getDomain()->getSizeWithHalo()[0] - 2, 0, 0);
                recvSizeCPU.set(1, solverCPU->getDomain()->getSizeWithHalo()[1], solverCPU->getDomain()->getSizeWithHalo()[2]);
                recvOriginGPU.set(solverGPU->getDomain()->getSizeWithHalo()[0] - 2, 0, 0);
                recvSizeGPU.set(1, solverGPU->getDomain()->getSizeWithHalo()[1], solverGPU->getDomain()->getSizeWithHalo()[2]);

                MPI_Wait(&recvRequests[recvIdx], MPI_STATUS_IGNORE);

                solverGPU->setDensityDistributions(recvOriginGPU, recvSizeGPU, CPU, communication[recvIdx].getDirection(), &recvBuffers->at(recvIdx)[NUM_LATTICE_VECTORS * recvSizeCPU.elements()], &streamsGPU->at(recvIdx));
                solverCPU->setDensityDistributions(recvOriginCPU, recvSizeCPU, CPU, communication[recvIdx].getDirection(), &recvBuffers->at(recvIdx)[0], &streamsCPU->at(recvIdx));
                GPU_ERROR_CHECK(cudaStreamSynchronize(streamsGPU->at(recvIdx)))
                GPU_ERROR_CHECK(cudaStreamSynchronize(streamsCPU->at(recvIdx)))

                recvIdx++;
            }
            if (static_cast<size_t>(sendIdx) < communication.size() && communication[sendIdx].getDirection() == BOTTOM)
            {
                sendOriginCPU.set(0, 0, 0);
                sendSizeCPU.set(solverCPU->getDomain()->getSizeWithHalo()[0], 1, solverCPU->getDomain()->getSizeWithHalo()[2]);

                solverCPU->getDensityDistributions(sendOriginCPU, sendSizeCPU, CPU, sendBuffers->at(sendIdx), &streamsCPU->at(sendIdx));
                GPU_ERROR_CHECK(cudaStreamSynchronize(streamsCPU->at(sendIdx)))

                MPI_Isend(
                        sendBuffers->at(sendIdx),
                        NUM_LATTICE_VECTORS * communication[sendIdx].getSendSize().elements(),
                        ((typeid(T) == typeid(float)) ? MPI_FLOAT : MPI_DOUBLE),
                        communication[sendIdx].getDstId(),
                        simulationStepCounter,
                        MPI_COMM_WORLD,
                        &sendRequests[sendIdx]);

                sendIdx++;
            }
            if (static_cast<size_t>(sendIdx) < communication.size() && communication[sendIdx].getDirection() == TOP)
            {
                sendOriginGPU.set(0, solverGPU->getDomain()->getSizeWithHalo()[1] - 1, 0);
                sendSizeGPU.set(solverGPU->getDomain()->getSizeWithHalo()[0], 1, solverGPU->getDomain()->getSizeWithHalo()[2]);

                solverGPU->getDensityDistributions(sendOriginGPU, sendSizeGPU, CPU, sendBuffers->at(sendIdx), &streamsGPU->at(sendIdx));
                GPU_ERROR_CHECK(cudaStreamSynchronize(streamsGPU->at(sendIdx)))

                MPI_Isend(
                        sendBuffers->at(sendIdx),
                        NUM_LATTICE_VECTORS * communication[sendIdx].getSendSize().elements(),
                        ((typeid(T) == typeid(float)) ? MPI_FLOAT : MPI_DOUBLE),
                        communication[sendIdx].getDstId(),
                        simulationStepCounter,
                        MPI_COMM_WORLD,
                        &sendRequests[sendIdx]);

                sendIdx++;
            }
            if (static_cast<size_t>(recvIdx) < communication.size() && communication[recvIdx].getDirection() == BOTTOM)
            {
                recvOriginCPU.set(0, 1, 0);
                recvSizeCPU.set(solverCPU->getDomain()->getSizeWithHalo()[0], 1, solverCPU->getDomain()->getSizeWithHalo()[2]);

                MPI_Wait(&recvRequests[recvIdx], MPI_STATUS_IGNORE);

                solverCPU->setDensityDistributions(recvOriginCPU, recvSizeCPU, CPU, communication[recvIdx].getDirection(), recvBuffers->at(recvIdx), &streamsCPU->at(recvIdx));
                GPU_ERROR_CHECK(cudaStreamSynchronize(streamsCPU->at(recvIdx)))

                recvIdx++;
            }
            if (static_cast<size_t>(recvIdx) < communication.size() && communication[recvIdx].getDirection() == TOP)
            {
                recvOriginGPU.set(0, solverGPU->getDomain()->getSizeWithHalo()[1] - 2, 0);
                recvSizeGPU.set(solverGPU->getDomain()->getSizeWithHalo()[0], 1, solverGPU->getDomain()->getSizeWithHalo()[2]);

                MPI_Wait(&recvRequests[recvIdx], MPI_STATUS_IGNORE);

                solverGPU->setDensityDistributions(recvOriginGPU, recvSizeGPU, CPU, communication[recvIdx].getDirection(), recvBuffers->at(recvIdx), &streamsGPU->at(recvIdx));
                GPU_ERROR_CHECK(cudaStreamSynchronize(streamsGPU->at(recvIdx)))

                recvIdx++;
            }
            if (static_cast<size_t>(sendIdx) < communication.size() && communication[sendIdx].getDirection() == BACK)
            {
                sendOriginCPU.set(0, 0, 0);
                sendSizeCPU.set(solverCPU->getDomain()->getSizeWithHalo()[0], solverCPU->getDomain()->getSizeWithHalo()[1], 1);
                sendOriginGPU.set(0, 0, 0);
                sendSizeGPU.set(solverGPU->getDomain()->getSizeWithHalo()[0], solverGPU->getDomain()->getSizeWithHalo()[1], 1);

                solverGPU->getDensityDistributions(sendOriginGPU, sendSizeGPU, CPU, &sendBuffers->at(sendIdx)[NUM_LATTICE_VECTORS * sendSizeCPU.elements()], &streamsGPU->at(sendIdx));
                solverCPU->getDensityDistributions(sendOriginCPU, sendSizeCPU, CPU, &sendBuffers->at(sendIdx)[0], &streamsCPU->at(sendIdx));
                GPU_ERROR_CHECK(cudaStreamSynchronize(streamsGPU->at(sendIdx)))
                GPU_ERROR_CHECK(cudaStreamSynchronize(streamsCPU->at(sendIdx)))

                MPI_Isend(
                        sendBuffers->at(sendIdx),
                        NUM_LATTICE_VECTORS * communication[sendIdx].getSendSize().elements(),
                        ((typeid(T) == typeid(float)) ? MPI_FLOAT : MPI_DOUBLE),
                        communication[sendIdx].getDstId(),
                        simulationStepCounter,
                        MPI_COMM_WORLD,
                        &sendRequests[sendIdx]);

                sendIdx++;
            }
            if (static_cast<size_t>(sendIdx) < communication.size() && communication[sendIdx].getDirection() == FRONT)
            {
                sendOriginCPU.set(0, 0, solverCPU->getDomain()->getSizeWithHalo()[2] - 1);
                sendSizeCPU.set(solverCPU->getDomain()->getSizeWithHalo()[0], solverCPU->getDomain()->getSizeWithHalo()[1], 1);
                sendOriginGPU.set(0, 0, solverGPU->getDomain()->getSizeWithHalo()[2] - 1);
                sendSizeGPU.set(solverGPU->getDomain()->getSizeWithHalo()[0], solverGPU->getDomain()->getSizeWithHalo()[1], 1);

                solverGPU->getDensityDistributions(sendOriginGPU, sendSizeGPU, CPU, &sendBuffers->at(sendIdx)[NUM_LATTICE_VECTORS * sendSizeCPU.elements()], &streamsGPU->at(sendIdx));
                solverCPU->getDensityDistributions(sendOriginCPU, sendSizeCPU, CPU, &sendBuffers->at(sendIdx)[0], &streamsCPU->at(sendIdx));
                GPU_ERROR_CHECK(cudaStreamSynchronize(streamsGPU->at(sendIdx)))
                GPU_ERROR_CHECK(cudaStreamSynchronize(streamsCPU->at(sendIdx)))

                MPI_Isend(
                        sendBuffers->at(sendIdx),
                        NUM_LATTICE_VECTORS * communication[sendIdx].getSendSize().elements(),
                        ((typeid(T) == typeid(float)) ? MPI_FLOAT : MPI_DOUBLE),
                        communication[sendIdx].getDstId(),
                        simulationStepCounter,
                        MPI_COMM_WORLD,
                        &sendRequests[sendIdx]);

                sendIdx++;
            }
            if (static_cast<size_t>(recvIdx) < communication.size() && communication[recvIdx].getDirection() == BACK)
            {
                recvOriginCPU.set(0, 0, 1);
                recvSizeCPU.set(solverCPU->getDomain()->getSizeWithHalo()[0], solverCPU->getDomain()->getSizeWithHalo()[1], 1);
                recvOriginGPU.set(0, 0, 1);
                recvSizeGPU.set(solverGPU->getDomain()->getSizeWithHalo()[0], solverGPU->getDomain()->getSizeWithHalo()[1], 1);

                MPI_Wait(&recvRequests[recvIdx], MPI_STATUS_IGNORE);

                solverGPU->setDensityDistributions(recvOriginGPU, recvSizeGPU, CPU, communication[recvIdx].getDirection(), &recvBuffers->at(recvIdx)[NUM_LATTICE_VECTORS * recvSizeCPU.elements()], &streamsGPU->at(recvIdx));
                solverCPU->setDensityDistributions(recvOriginCPU, recvSizeCPU, CPU, communication[recvIdx].getDirection(), &recvBuffers->at(recvIdx)[0], &streamsCPU->at(recvIdx));

                recvIdx++;
            }
            if (static_cast<size_t>(recvIdx) < communication.size() && communication[recvIdx].getDirection() == FRONT)
            {
                recvOriginCPU.set(0, 0, solverCPU->getDomain()->getSizeWithHalo()[2] - 2);
                recvSizeCPU.set(solverCPU->getDomain()->getSizeWithHalo()[0], solverCPU->getDomain()->getSizeWithHalo()[1], 1);
                recvOriginGPU.set(0, 0, solverGPU->getDomain()->getSizeWithHalo()[2] - 2);
                recvSizeGPU.set(solverGPU->getDomain()->getSizeWithHalo()[0], solverGPU->getDomain()->getSizeWithHalo()[1], 1);

                MPI_Wait(&recvRequests[recvIdx], MPI_STATUS_IGNORE);

                solverGPU->setDensityDistributions(recvOriginGPU, recvSizeGPU, CPU, communication[recvIdx].getDirection(), &recvBuffers->at(recvIdx)[NUM_LATTICE_VECTORS * recvSizeCPU.elements()], &streamsGPU->at(recvIdx));
                solverCPU->setDensityDistributions(recvOriginCPU, recvSizeCPU, CPU, communication[recvIdx].getDirection(), &recvBuffers->at(recvIdx)[0], &streamsCPU->at(recvIdx));

                recvIdx++;
            }
#endif
            sendOriginCPU.set(0, solverCPU->getDomain()->getSizeWithHalo()[1] - 1, 0);
            recvOriginCPU.set(0, solverCPU->getDomain()->getSizeWithHalo()[1] - 2, 0);
            sendOriginGPU.set(0, 0, 0);
            recvOriginGPU.set(0, 1, 0);
            sendSizeCPU.set(solverCPU->getDomain()->getSizeWithHalo()[0], 1, solverCPU->getDomain()->getSizeWithHalo()[2]);
            recvSizeCPU.set(solverCPU->getDomain()->getSizeWithHalo()[0], 1, solverCPU->getDomain()->getSizeWithHalo()[2]);
            sendSizeGPU.set(solverGPU->getDomain()->getSizeWithHalo()[0], 1, solverGPU->getDomain()->getSizeWithHalo()[2]);
            recvSizeGPU.set(solverGPU->getDomain()->getSizeWithHalo()[0], 1, solverGPU->getDomain()->getSizeWithHalo()[2]);
            dimCPU = solverCPU->getDomain()->getSizeWithHalo();
            dimGPU = solverGPU->getDomain()->getSizeWithHalo();

            solverGPU->setDensityDistributions(recvSizeGPU, sendOriginCPU, dimCPU, CPU, recvOriginGPU, BOTTOM, solverCPU->getDensityDistributions(), &CPUtoGPUStream);
            solverCPU->setDensityDistributions(recvSizeCPU, sendOriginGPU, dimGPU, GPU, recvOriginCPU, TOP, solverGPU->getDensityDistributions(), &GPUtoCPUStream);
        }
#ifdef USE_MPI

        /*
         * Explicit synchronization point after all MPI send buffers can be safely
         * re-used again which means: Communication with neighbors has finished.
         */
        MPI_Waitall(communication.size(), sendRequests, MPI_STATUS_IGNORE);
#endif
    }
    /*
     * Implicit synchronization point after OpenMP parallel region after inner
     * region of the CPU has been processed.
     */

    /*
     * Explicit synchronization point after inner region of the GPU has been
     * processed.
     */
    GPU_ERROR_CHECK(cudaStreamSynchronize(innerStream))
    /*
     * Explicit synchronization point to complete communication between CPU and
     * GPU part of a subdomain.
     */
    if (solverCPU->getDomain()->hasDomain() && solverGPU->getDomain()->hasDomain())
    {
        GPU_ERROR_CHECK(cudaStreamSynchronize(GPUtoCPUStream))
        GPU_ERROR_CHECK(cudaStreamSynchronize(CPUtoGPUStream))
    }

    if (configuration->doLogging)
    {
        std::stringstream loggingFileName;
        loggingFileName << configuration->loggingOutputDir << "/log_" << id << ".txt";
        std::ofstream loggingFile(loggingFileName.str().c_str(), std::ios::out | std::ios::app);
        if (loggingFile.is_open())
        {
            loggingFile << "Beta step successful." << std::endl;
            loggingFile << "--------------------------------------" << std::endl;
            loggingFile.close();
        } else {
            std::cerr << "----- CController<T>::stepBeta() -----" << std::endl;
            std::cerr << "There is no open file to write logs." << std::endl;
            std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
            std::cerr << "--------------------------------------" << std::endl;

            exit (EXIT_FAILURE);
        }
    }
}

template <class T>
void CController<T>::computeNextStep()
{
    if (simulationStepCounter & 1) {
        stepBeta();
    } else {
        stepAlpha();
    }
    simulationStepCounter++;
}

template <class T>
void CController<T>::setDrivenCavitySzenario()
{
    if (configuration->doLogging)
    {
        std::stringstream loggingFileName;
        loggingFileName << configuration->loggingOutputDir << "/log_" << id << ".txt";
        std::ofstream loggingFile(loggingFileName.str().c_str(), std::ios::out | std::ios::app);
        if (loggingFile.is_open())
        {
            loggingFile << "----- CController<T>::setDrivenCavitySzenario() -----" << std::endl;
            loggingFile << "Cells where velocity injection takes place due to driven cavity scenario are marked accordingly." << std::endl;
            loggingFile << "-----------------------------------------------------" << std::endl;
            loggingFile.close();
        } else {
            std::cerr << "----- CController<T>::setDrivenCavitySzenario() -----" << std::endl;
            std::cerr << "There is no open file to write logs." << std::endl;
            std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
            std::cerr << "-----------------------------------------------------" << std::endl;

            exit (EXIT_FAILURE);
        }
    }

    CVector<3, int> origin(1, solverGPU->getDomain()->hasDomain() ? solverGPU->getDomain()->getSize()[1] : solverCPU->getDomain()->getSize()[1], 1);
    CVector<3, int> size(domain.getSize()[0], 1, domain.getSize()[2]);

    if (configuration->doLogging)
    {
        std::stringstream loggingFileName;
        loggingFileName << configuration->loggingOutputDir << "/log_" << id << ".txt";
        std::ofstream loggingFile(loggingFileName.str().c_str(), std::ios::out | std::ios::app);
        if (loggingFile.is_open())
        {
            loggingFile << "origin: " << origin << std::endl;
            loggingFile << "size:   " << size << std::endl;
            loggingFile << "-----------------------------------------------------" << std::endl;
            loggingFile.close();
        } else {
            std::cerr << "----- CController<T>::setDrivenCavitySzenario() -----" << std::endl;
            std::cerr << "There is no open file to write logs." << std::endl;
            std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
            std::cerr << "-----------------------------------------------------" << std::endl;

            exit (EXIT_FAILURE);
        }
    }

    Flag* src = new Flag[size.elements()];

    for (int i = 0; i < size.elements(); i++)
    {
        src[i] = VELOCITY_INJECTION;
    }

    if (solverGPU->getDomain()->hasDomain())
        solverGPU->setFlags(origin, size, CPU, src);
    else
        solverCPU->setFlags(origin, size, CPU, src);

    delete[] src;
}

/*
 * This function starts the simulation for the particular subdomain corresponded to
 * this class.
 */
template <class T>
void CController<T>::run()
{
    int usedDataSize;
    timeval start, end;
    T elapsed;

    usedDataSize = 0;

    if (configuration->doLogging)
    {
        std::stringstream loggingFileName;
        loggingFileName << configuration->loggingOutputDir << "/log_" << id << ".txt";
        std::ofstream loggingFile(loggingFileName.str().c_str(), std::ios::out | std::ios::app);
        if (loggingFile.is_open())
        {
            loggingFile << "----- CController<T>::run() -----" << std::endl;
            loggingFile.close();
        } else {
            std::cerr << "----- CController<T>::run() -----" << std::endl;
            std::cerr << "There is no open file to write logs." << std::endl;
            std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
            std::cerr << "---------------------------------" << std::endl;

            exit (EXIT_FAILURE);
        }
    }

    if (configuration->doBenchmark)
    {
        /*
         * Density distributions are read and written
         */
        usedDataSize += NUM_LATTICE_VECTORS * 2 * sizeof(T);
        /*
         * Flag is read
         */
        usedDataSize += 1 * sizeof(Flag);


        if (configuration->doVisualization || configuration->doValidation)
            /*
             * Velocity (3) and density (1) are written
             */
            usedDataSize += 4 * sizeof(T);
    }

    if (configuration->doVisualization)
        visualization->render(0);

    if (configuration->doBenchmark)
        gettimeofday(&start, NULL);

    for (int i = 0; i < configuration->loops || configuration->loops < 0; i++) {
        if (configuration->doLogging)
        {
            std::stringstream loggingFileName;
            loggingFileName << configuration->loggingOutputDir << "/log_" << id << ".txt";
            std::ofstream loggingFile(loggingFileName.str().c_str(), std::ios::out | std::ios::app);
            if (loggingFile.is_open())
            {
                loggingFile << "Do iteration " << i << ":" << std::endl;
                loggingFile.close();
            } else {
                std::cerr << "----- CController<T>::run() -----" << std::endl;
                std::cerr << "There is no open file to write logs." << std::endl;
                std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
                std::cerr << "---------------------------------" << std::endl;

                exit (EXIT_FAILURE);
            }
        }

        computeNextStep();

        if (configuration->doVisualization)
            visualization->render(simulationStepCounter);
            
        if (configuration->doLogging)
        {
            std::stringstream loggingFileName;
            loggingFileName << configuration->loggingOutputDir << "/log_" << id << ".txt";
            std::ofstream loggingFile(loggingFileName.str().c_str(), std::ios::out | std::ios::app);
            if (loggingFile.is_open())
            {
                loggingFile << "Iteration " << i << " successful." << std::endl;
                loggingFile.close();
            } else {
                std::cerr << "----- CController<T>::run() -----" << std::endl;
                std::cerr << "There is no open file to write logs." << std::endl;
                std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
                std::cerr << "---------------------------------" << std::endl;

                exit (EXIT_FAILURE);
            }
        }
    }

    if (configuration->doBenchmark)
    {
        gettimeofday(&end, NULL);
        elapsed = (T)(end.tv_sec - start.tv_sec) + (T)(end.tv_usec - start.tv_usec) * (T)0.000001;

#ifdef USE_MPI
        int rank, numOfRanks;

        MPI_Comm_rank(MPI_COMM_WORLD, &rank);
        MPI_Comm_size(MPI_COMM_WORLD, &numOfRanks);
        
        if (rank == 0)
        {
#endif
            T iterationsPerSecond = (T)(configuration->loops) / elapsed;
            T lups = iterationsPerSecond * (T)configuration->domainSize[0] * (T)configuration->domainSize[1] * (T)configuration->domainSize[2] * (T)0.000000001;
            T bandwidth = lups * (T)usedDataSize;

            std::stringstream benchmarkFileName;
            benchmarkFileName << configuration->benchmarkOutputDir << "/benchmark_";
#ifdef USE_MPI
            benchmarkFileName << numOfRanks << "_";
#endif
            benchmarkFileName << configuration->domainSize[0] << "x" << configuration->domainSize[1] << "x" << configuration->domainSize[2] << "_" << configuration->numOfSubdomains[0] << "x" << configuration->numOfSubdomains[1] << "x" << configuration->numOfSubdomains[2] << "_" << configuration->CPUSubdomainRatio <<".txt";
        
            std::ofstream benchmarkFile(benchmarkFileName.str().c_str(), std::ios::out);
            if (benchmarkFile.is_open())
            {
                benchmarkFile << "loops:           " << configuration->loops << std::endl;
                benchmarkFile << "time:            " << elapsed << "s" << std::endl;
                benchmarkFile << "iterations:      " << iterationsPerSecond << "s^-1" << std::endl;
                benchmarkFile << "lattice updates: " << lups << "GLUPS" << std::endl;
                benchmarkFile << "bandwidth:       " << bandwidth << "GBytes/s" << std::endl;
                benchmarkFile.close();
            } else {
                std::cerr << "----- CController<T>::run() -----" << std::endl;
                std::cerr << "There is no open file to write benchmark results." << std::endl;
                std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
                std::cerr << "---------------------------------" << std::endl;

                exit (EXIT_FAILURE);
            }
#ifdef USE_MPI
        }
#endif
    }

    if (configuration->doLogging)
    {
        std::stringstream loggingFileName;
        loggingFileName << configuration->loggingOutputDir << "/log_" << id << ".txt";
        std::ofstream loggingFile(loggingFileName.str().c_str(), std::ios::out | std::ios::app);
        if (loggingFile.is_open())
        {
            loggingFile << "---------------------------------" << std::endl;
            loggingFile.close();
        } else {
            std::cerr << "----- CController<T>::run() -----" << std::endl;
            std::cerr << "There is no open file to write logs." << std::endl;
            std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
            std::cerr << "---------------------------------" << std::endl;

            exit (EXIT_FAILURE);
        }
    }
}

template <class T>
int CController<T>::getId() {
    return id;
}

template <class T>
CDomain<T>* CController<T>::getDomain() {
    return &domain;
}

template <class T>
CLbmSolverCPU<T>* CController<T>::getSolverCPU() {
    return solverCPU;
}

template <class T>
CLbmSolverGPU<T>* CController<T>::getSolverGPU() {
    return solverGPU;
}

template class CController<double>;
template class CController<float>;

