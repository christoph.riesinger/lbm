Features
========
This C++ code implements the lattice Boltzmann method with the following features:
- Supports execution on CPU and GPU, also simultaneously (heterogeneous computing)
- CPU kernels parallelized with OpenMP, GPU kernels implemented with CUDA
- Parallelized with MPI (hybrid implementation)
- Communication hided with computation
- Measured scalability up to 2048 GPUs and 24576 CPU cores
- Export of simulation data as NetCDF (also parallel NetCDF) or VTK

Prerequisites
=============
Mandatory
---------
*lbm* has the following dependencies:
- **CUDA Toolkit**: *lbm* can entirely be run on the CPU if no work is explicitly assigned to the GPU. However, even with such a configuration, *lbm* requires functionality from CUDA libraries (e.g. `cudaMemcpy3D()`). Hence, `cuda_runtime.h` is necessary during compile time and the CUDA runtime library is mandatory during link and run time.
- **NetCDF**: *lbm* can export files also as VTK. However, NetCDF libraries are mandatory during compile, link, and run times. Serial and parallel NetCDF are exported.

Optional
--------
*lbm* has the following dependencies if explicitly requested:
- **MPI**

Build
=====
*lbm* uses CMake as build system.

CMake options
-------------
| Option    | Type      | Default | Description                                                                     |
| --------- | --------- | ------- | ------------------------------------------------------------------------------- |
| `USE_MPI` | `boolean` | `OFF`   | Enables distributed memory parallelization with MPI. MPI is detected via CMake. |

Build example
-------------
This is a build example using make under Linux.
```shell
mkdir build
cd build
cmake .. -DUSE_MPI=ON

make -j$(nproc)
```

Running
=======
A valid XML configuration file specifying the physics, domain, and parallel setup of the simulation has to be passed when starting the application. It's not possible to pass these parameters via command line. [configurations/default.xml](configurations/default.xml) is an example for such an XML configuration file. All listed parameters in `default.xml` have to be provided by the user. Additional parameters are ignored.

Parameters for XML configuration file
-------------------------------------
| Parameter               | Description                                                                                                 |
| ----------------------- | ----------------------------------------------------------------------------------------------------------- |
| `<velocity>`            | Velocity im m/s of the driving lid. Only *x* value is considered                                            |
| `<viscosity>`           | Kinematic viscosity. If a negative value is passed, an appropriate viscosity for Re = 1000 is set           |
| `<domain-size>`         | Domain size in lattice cells                                                                                |
| `<domian-length>`       | Domain length in m. Ratio of directions has to fit the ration of the domain size                            |
| `<subdomain-num>`       | Number of subdomains. Product has to match the number of launched MPI processes                             |
| `<cpu-subdomain-ratio>` | Share of the CPU-part of a subdomain to the total size of the subdomain. Ratio refers to the *y*-direction  |
| `<loops>`               | Number of timesteps to run. An even number has to be provided due to alpha- and beta-steps                  |
| `<timestep>`            | Timestep size in s. If CFL condition is not satisfied, timestep size is automatically adapted (see logging) |
| `<benchmark>`           | Option to output benchmark values such as memory bandwidth utilization and LUPS rate in specified folder    |
| `<logging>`             | Option to output per process log files in specified folder                                                  |
| `<validation>`          | Option to validate multi-process results with single-process results. Differences are stored in specified folder. Currently not implemented anymore, thus has no effect |
| `<visualization>`       | Option to store simulation data for visualization in specified folder. `<rate>` specifies the number of timesteps after which a snapshot is returned. netCDF or VTK output has to be hard coded |
| `<block-configuration>` | Blocking size of alpha- and beta-kernel on CPU to increase cache efficiency                                 |
| `<grid-configuration>`  | Parallel setup of alpha- and beta-kernel on GPU                                                             |

Run examples
------------
Running lbm could look as follows:
`lbm configurations/default.xml`

Running lbm with MPI could look as follows:
`mpiexec -n 2 lbm configurations/default.xml`

Remarks
========
- *lbm* always requires a CUDA installation (but no GPUs) to be compiled even if CPU-only experiments should be carried out. The reason is the usage of the `cudaMemcpy3D()` function even for host memory to host memory copies.
- The only necessary library (besides CUDA) for *lbm* is (parallel) netCDF. It is also required if no or only VTK export is done.
- *lbm* brings code for an XML importer ([external/tinyxml2](external/tinyxml2/tinyxml2.cpp)) and a VTK exporter ([src/libvis/CLbmVisualizationVTK](src/libvis/CLbmVisualizationVTK.cpp)).
- There is no OpenCL code.